package main

import (
	"fmt"
	"os"
	"simple"
	"strconv"
)

func main() {
	args := os.Args

	if len(args) != 4 {
		ErrorInfo()
		return
	}

	v1, error1 := strconv.Atoi(args[2])
	v2, error2 := strconv.Atoi(args[2])

	if error1 != nil || error2 != nil {
		fmt.Println("This command is Error.")
		return
	}

	ret := simple.Add(v1, v2)
	fmt.Println("Result: ", ret)
}

var ErrorInfo = func() {
	fmt.Println("This command is Error.")
}
