package main

import (
	"fmt"
	"simple"
	"strconv"
	"strings"
)

func readLine(line string, name string) {
	idxStart := strings.Index(line, "gyqx.unit.")
	tStart := strings.Index(line, "2021-12")
	idxEnd := strings.LastIndex(line, "耗时：")
	idxCend := strings.LastIndex(line, "结束")
	if idxStart > -1 && idxEnd > -1 {
		timeByte := []byte(line)[idxEnd+9 : len(line)-2]
		ctrlByte := []byte(line)[idxStart:idxCend]
		dateByte := []byte(line)[tStart : tStart+23]

		time, err := strconv.Atoi(string(timeByte))
		if err != nil {
			fmt.Printf("filepath.Walk() error: %v\n", err)
		}
		if time > 2000 {
			fmt.Printf(name + "[" + string(dateByte) + "]" + string(ctrlByte) + "<" + string(timeByte) + ">\n")
		}
	}
}

func main() {
	process := simple.FileProcess{Root: "E:/logs-xxxx", Callback: readLine}
	process.StartWalk()
}
