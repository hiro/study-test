package simple

type BaseUtil struct {
	dataBase map[string]BaseEntity
}

type BaseEntity struct {
	uid string
}

func (e *BaseUtil) save(b BaseEntity) {
	if e.dataBase == nil {
		e.dataBase = make(map[string]BaseEntity)
	}
	e.dataBase[b.uid] = b
}

func (e *BaseUtil) delete(uid string) bool {
	if e.dataBase == nil {
		return false
	}
	delete(e.dataBase, uid)
	return true
}

func (e *BaseUtil) get(uid string) (entity BaseEntity) {
	if e.dataBase == nil {
		return
	}
	obj, ok := e.dataBase[uid]
	if ok {
		entity = obj
	}
	return
}
