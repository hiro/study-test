package main

import (
	"fmt"
)

func main() {
	fmt.Println("你好，小瓜瓜！")

	var result = isPrime(1000)
	fmt.Println(result)
}

/**
 * 冒泡排序：改进版
 * var balance = []int{100, 2, 3, 7, 50}
 * bubbleSort(balance)
 */
func bubbleSort(arr []int) {
	for i := len(arr) - 1; i > 0; i-- {
		for j := 0; j < i; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
			}
		}
	}
}

/**
 * 判断一个数是不是质数
 */
func isPrime(num int) bool {
	if num < 1 {
		return false
	}
	slice := make([]bool, num+1)
	slice[2] = true
	for i := 2; i < num+1; i++ {
		slice[i] = true
	}
	for i := 2; i < num+1; i++ {
		if slice[i] == true {
			for j := 2 * i; j < num+1; j = j + i {
				slice[j] = false
			}
		}
	}
	for i, v := range slice {
		if v {
			fmt.Println(i)
		}
	}
	return slice[num]
}
