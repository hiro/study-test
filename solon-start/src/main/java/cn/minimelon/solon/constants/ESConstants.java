package cn.minimelon.solon.constants;

public class ESConstants {
    public static final String ANALYZE = "/_analyze";
    public static final String IK_SMART = "ik_smart";
    public static final String CN_WORD = "CN_WORD";
    public static final String ENGLISH = "ENGLISH";
    public static final String LETTER = "LETTER";

    public static final String ES_IDX_COMP = "bas_company_info";
}
