package cn.minimelon.solon.config;

import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class ThreadPool {
    @Bean
    public ExecutorService initPool() {
        return Executors.newFixedThreadPool(6);
    }
}
