package cn.minimelon.solon.config;

import com.zaxxer.hikari.HikariDataSource;
import org.beetl.core.GroupTemplate;
import org.beetl.core.resource.StringTemplateResource;
import org.beetl.core.resource.StringTemplateResourceLoader;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
public class JdbcConfig {
    @Bean(name="master", typed=true)
    public DataSource master(@Inject("${jdbc.master}") HikariDataSource ds) {
        return ds;
    }

    @Bean(name="writer", typed=true)
    public DataSource writer(@Inject("${jdbc.writer}") HikariDataSource ds) {
        return ds;
    }

    @Bean
    public GroupTemplate strTemplate() throws IOException {
        StringTemplateResourceLoader resourceLoader = new StringTemplateResourceLoader();
        org.beetl.core.Configuration cfg = org.beetl.core.Configuration.defaultConfiguration();
        return new GroupTemplate(resourceLoader, cfg);
    }
}
