package cn.minimelon.solon.config;

import org.noear.solon.annotation.Condition;
import org.noear.solon.annotation.Configuration;

@Condition(onProperty = "elastic.url")
@Configuration
public class ElasticConfig {

}
