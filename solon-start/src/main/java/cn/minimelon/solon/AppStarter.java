package cn.minimelon.solon;

import org.beetl.sql.core.SQLManagerBuilder;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.noear.solon.Solon;

public class AppStarter {
    public static void main(String[] args) {
        Solon.start(AppStarter.class, args, app -> {
            app.onEvent(SQLManagerBuilder.class, c -> {
                // 启用开发或调试模式（可以打印sql）
                c.setNc(new UnderlinedNameConversion());
                c.addInterDebug();
            });
        });
    }
}
