package cn.minimelon.solon.mapper.analyze;

import cn.minimelon.solon.domain.analyze.CompanyGroupMax;
import org.beetl.sql.mapper.BaseMapper;

public interface CompanyGroupMaxMapper extends BaseMapper<CompanyGroupMax> {
}
