package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.FusionQueryVO;
import cn.minimelon.solon.domain.system.FusionInvoiceDetail;
import cn.minimelon.solon.domain.system.FusionInvoiceDetailVO;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface Spd2InvoiceMapper extends BaseMapper<FusionInvoiceDetail> {
    @Select
    @Template("select iht.id as detailId,inv.invoice_code,inv.invoice_number,inv.settle_month,inv.status as tbStatus," +
            "  hobd.bill_id as settleOrderNo,inv.hos_id,hob.hos_name,inv.branch_id,iht.hos_goods_id," +
            "  hobd.hos_goods_name as goodsName,iht.hos_goods_id as goods_code,hobd.goods_gg,hobd.batch_code," +
            "  hobd.hos_unit as unit,iht.price,iht.invoice_qty,iht.invoice_amount,hobd.prov_id," +
            "  hobd.prov_name,hobd.sub_prov_id,hobd.sub_prov_name,inv.create_date as invoicingTime," +
            "  inv.last_update_datetime as operationTime, iht.fill_date as create_time" +
            " from spd.invoice_hobd_temp iht" +
            " left join spd.invoice inv on iht.invoice_id=inv.id" +
            " left join spd.hos_out_balance_detail hobd on iht.hobd_id=hobd.id" +
            " left join spd.hos_out_balance hob ON hob.id=hobd.pid" +
            " where inv.settle_month = #{month} order by detailId asc limit #{skip}, 10000"
    )
    List<FusionInvoiceDetailVO> selectListByCond(FusionQueryVO query);

    @Select
    @Template("select count(1) as cnum" +
            " from spd.invoice_hobd_temp iht" +
            " join spd.invoice inv on iht.invoice_id=inv.id" +
            " where inv.settle_month = #{month}")
    Integer selectCountByCond(FusionQueryVO query);
}
