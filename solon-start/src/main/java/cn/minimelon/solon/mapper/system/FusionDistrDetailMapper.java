package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.DistrDetail;
import org.beetl.sql.mapper.BaseMapper;

public interface FusionDistrDetailMapper extends BaseMapper<DistrDetail> {
}
