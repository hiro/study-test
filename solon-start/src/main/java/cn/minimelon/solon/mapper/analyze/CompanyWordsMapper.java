package cn.minimelon.solon.mapper.analyze;

import cn.minimelon.solon.domain.analyze.CompanyWords;
import org.beetl.sql.mapper.BaseMapper;

public interface CompanyWordsMapper extends BaseMapper<CompanyWords> {
}
