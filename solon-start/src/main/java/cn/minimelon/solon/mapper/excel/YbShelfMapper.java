package cn.minimelon.solon.mapper.excel;

import cn.minimelon.solon.domain.excel.YbShelf;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Template;

public interface YbShelfMapper extends BaseMapper<YbShelf> {

    @Template("update yb_shelf set id = #{newId} where id = #{oldId}")
    void updateId(String oldId, String newId);
}
