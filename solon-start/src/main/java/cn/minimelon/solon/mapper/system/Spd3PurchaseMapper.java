package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.Spd2DistrBill;
import cn.minimelon.solon.domain.system.Spd3PurchaseDetail;
import cn.minimelon.solon.domain.system.SysConfig;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Template;
import org.beetl.sql.mapper.annotation.Update;

import java.util.List;

public interface Spd3PurchaseMapper extends BaseMapper<SysConfig> {

    @Select
    @Template("select a.id, a.pid, a.ext_info, a.create_time, b.hos_goods_code as goods_code, b.goods_name, b.goods_spec as goods_gg"
            + " from mcms_purchase_detail a join mcms_goods_info b on a.hos_goods_id = b.id"
            + " where a.create_time >= date_sub(now(), interval 31 day)")
    List<Spd3PurchaseDetail> selectUpdateList();

    @Update
    @Template("update mcms_purchase_detail set version=version+1, last_modified=now(), ext_info=#{extInfo} where id = #{id}")
    Integer updatePurDetail(Spd3PurchaseDetail update);

    @Update
    @Template("update mcms_purchase set version=version+1, last_modified=now() where create_time >= date_sub(now(), interval 31 day)")
    Integer updatePurBill();

    @Select
    @Template("select a.id, a.fill_date as create_date, a.bill_relation_json, b.pur_bill_id, b.purconfirm_bill_id, c.fill_date as pur_date from distr_bill a" +
            " left join distr_bill_list b on b.pid = a.id and b.`row_number` = 1" +
            " left join purchase c on c.id = b.pur_bill_id" +
            " where a.fill_date < date_sub(now(), interval 61 day)")
    List<Spd2DistrBill> selectDistrBillList();

    @Update
    @Template("insert into sysr_print (id, pcode, `code`, direction, width, height, print_file_name, print_file, print_style_img, enable_flag, remark," +
            "  version, create_time, create_user, last_modified, last_modified_user)" +
            " select" +
            "   replace(id, 'h00bc', #{hosId}), pcode, replace(`code`, 'h00bc', #{hosId}), direction, width, height," +
            "   replace(print_file_name, 'h00bc', #{hosId}), print_file, print_style_img, enable_flag," +
            "   replace(remark, '南华附一', #{hosName})," +
            "   version, now(), '0000', now(), '0000'" +
            " from sysr_print where id = 'SYS_PRINT_DBP_h00bc'")
    int insertPrintInfo(String hosId, String hosName);
}
