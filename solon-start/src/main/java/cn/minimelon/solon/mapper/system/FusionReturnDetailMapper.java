package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.FusionQueryVO;
import cn.minimelon.solon.domain.system.ReturnDetail;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface FusionReturnDetailMapper extends BaseMapper<ReturnDetail> {
    @Select
    @Template("select count(1) as cnum" +
            " from fusion_return_detail" +
            " where create_time >= #{startTime} and create_time < #{endTime}")
    Integer selectCountByCond(FusionQueryVO query);

    @Select
    @Template("select detail_id" +
            " from fusion_return_detail" +
            " where detail_id in ( #{join(ids)} )")
    List<String> selectByDetailIds(List<String> ids);
}
