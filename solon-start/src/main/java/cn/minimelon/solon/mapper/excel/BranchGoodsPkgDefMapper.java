package cn.minimelon.solon.mapper.excel;

import cn.minimelon.solon.domain.excel.BranchGoodsPkgDef;
import org.beetl.sql.mapper.BaseMapper;

public interface BranchGoodsPkgDefMapper extends BaseMapper<BranchGoodsPkgDef> {
}
