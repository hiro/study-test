package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.DistrDetail;
import cn.minimelon.solon.domain.system.FusionQueryVO;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface Spd2DistrDetailMapper extends BaseMapper<DistrDetail> {
    @Select
    @Template("select dbl.id,dbl.pid order_id,db.purchase_company_id hos_id,db.purchase_company_name hos_name," +
            "  db.branch_id,db.prov_id,db.prov_name,db.sub_prov_id,db.bill_id order_no,db.fill_date distr_time," +
            "  dbl.hos_goods_id goods_id,dbl.hos_goods_code goods_code,dbl.hos_goods_name goods_name," +
            "  dbl.goods_gg,hgi.hos_mfrs_name mfrs_name,dbl.send_price distr_price,dbl.send_qty distr_qty," +
            "  dbl.batch_code,dbl.expdt_end_date expdt_date,db.pur_mode,hgi.unit,2 as hos_version" +
            " from spd.distr_bill db" +
            " inner join spd.distr_bill_list dbl on dbl.pid=db.id" +
            " inner join spd.hos_goods_info hgi on hgi.id=dbl.hos_goods_id" +
            " where db.fill_date >= #{startTime} and db.fill_date < #{endTime}")
    List<DistrDetail> selectListByCond(FusionQueryVO query);
}
