package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.BaseCompanyInfo;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Template;

public interface CompanyInfoMapper extends BaseMapper<BaseCompanyInfo> {
    @Select
    @Template("select cname from bas_company_info where id = #{id}")
    String selectCompNameById(@Param("id") String id);
}
