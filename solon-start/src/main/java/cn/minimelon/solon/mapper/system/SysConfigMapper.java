package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.SysConfig;
import org.beetl.sql.mapper.BaseMapper;

public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
