package cn.minimelon.solon.mapper.excel;

import cn.minimelon.solon.domain.excel.DeptGoodsDTO;
import cn.minimelon.solon.domain.excel.YbGoods;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Sql;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface YbGoodsMapper extends BaseMapper<YbGoods> {

    @Template("insert into yb_goods(" +
            " id,hos_id,logic_id,goods_code,goods_id,goods_name,goods_gg,goods_di," +
            " unit,mfrs_id,mfrs_name,made,spd_goods_code,pur_mode,can_purchase,yb_flag," +
            " min_qty,max_qty,tb_status,create_user,create_time,last_modified,last_modified_user,version)" +
            " values (#{id},#{hosId},#{logicId},#{goodsCode},#{goodsId},#{goodsName},#{goodsGg},#{goodsDi}," +
            " #{unit},#{mfrsId},#{mfrsName},#{made},#{spdGoodsCode},#{purMode},#{canPurchase},#{ybFlag}," +
            " #{minQty},#{maxQty},#{tbStatus},#{createUser},#{createTime},#{lastModified},#{lastModifiedUser},#{version})")
    int insertX(YbGoods ybGoods);

    @Sql("select * from v_update_dept_goods")
    List<DeptGoodsDTO> selectDeptGoodsList();
}
