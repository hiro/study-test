package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.FusionInvoiceDetail;
import cn.minimelon.solon.domain.system.FusionQueryVO;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface FusionInvoiceDetailMapper extends BaseMapper<FusionInvoiceDetail> {

    @Select
    @Template("select count(1) as cnum" +
            " from fusion_invoice_detail" +
            " where settle_month = #{month}")
    Integer selectCountByCond(FusionQueryVO query);

    @Select
    @Template("select detail_id" +
            " from fusion_invoice_detail" +
            " where detail_id in ( #{join(ids)} )")
    List<String> selectByDetailIds(List<String> ids);
}
