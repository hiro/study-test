package cn.minimelon.solon.mapper.analyze;

import cn.minimelon.solon.domain.analyze.CompanyManual;
import org.beetl.sql.mapper.BaseMapper;

public interface CompanyManualMapper extends BaseMapper<CompanyManual> {
}
