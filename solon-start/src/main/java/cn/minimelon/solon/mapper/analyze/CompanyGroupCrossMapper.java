package cn.minimelon.solon.mapper.analyze;

import cn.minimelon.solon.domain.analyze.CompanyGroupCross;
import org.beetl.sql.mapper.BaseMapper;

public interface CompanyGroupCrossMapper extends BaseMapper<CompanyGroupCross> {
}
