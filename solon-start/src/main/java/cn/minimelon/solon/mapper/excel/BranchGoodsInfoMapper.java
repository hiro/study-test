package cn.minimelon.solon.mapper.excel;

import cn.minimelon.solon.domain.excel.BranchGoodsInfo;
import org.beetl.sql.mapper.BaseMapper;

public interface BranchGoodsInfoMapper extends BaseMapper<BranchGoodsInfo> {
}
