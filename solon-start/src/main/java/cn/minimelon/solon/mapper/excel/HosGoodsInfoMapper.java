package cn.minimelon.solon.mapper.excel;

import cn.minimelon.solon.domain.excel.HosGoodsInfo;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface HosGoodsInfoMapper extends BaseMapper<HosGoodsInfo> {

    @Template("select a.*, b.di_package as goods_di from hos_goods_info a" +
            " left join bas_goods_package b on b.bas_goods_id = a.spd_goods_code and b.di_is_main = 1" +
            " where a.id in ( #{join(ids)} )")
    List<HosGoodsInfo> selectExtByIds(List<String> ids);
}
