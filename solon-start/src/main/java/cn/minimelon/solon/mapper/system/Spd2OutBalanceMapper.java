package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.FusionOutBalanceDetail;
import cn.minimelon.solon.domain.system.FusionOutBalanceDetailVO;
import cn.minimelon.solon.domain.system.FusionQueryVO;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface Spd2OutBalanceMapper extends BaseMapper<FusionOutBalanceDetail> {
    @Select
    @Template("select hobd.id as detailId,hob.hos_id,hob.hos_name,hob.spd_code as branchId," +
            "  hob.bill_id as settleOrderNo,hob.fill_date as account_date,dbl.bill_id as distrOrderNo," +
            "  dbl.send_price as distrPrice,dbl.send_qty as distrQty,hobd.hos_goods_id,hobd.goods_code," +
            "  hobd.hos_goods_name as goods_name,hobd.goods_gg,hobd.status as tb_status,hobd.mfrs_id," +
            "  hobd.price as settlePrice,hobd.settle_qty,hobd.invoice_qty,hobd.settle_amount," +
            "  hobd.invoice_qty*hobd.price as invoice_amount,hobd.prov_id,hobd.prov_name,hobd.sub_prov_id," +
            "  hobd.sub_prov_name,hobd.last_update_datetime as operationTime, hob.fill_date as create_time" +
            " from spd.hos_out_balance_detail hobd" +
            " left join spd.hos_out_balance hob on hob.id=hobd.pid" +
            " left join spd.distr_bill_list dbl on dbl.hos_goods_id=hobd.hos_goods_id" +
            "     and dbl.bill_id=hobd.batch_id and hobd.batch_code=dbl.batch_code and hobd.expdt_end_date=dbl.expdt_end_date" +
            "     and ifnull(dbl.check_code,'0')!='-10000'" +
            " where hob.fill_date >= #{startTime} and hob.fill_date < #{endTime} order by detailId limit #{skip}, #{pageSize}")
    List<FusionOutBalanceDetailVO> selectListByCond(FusionQueryVO query);

    @Select
    @Template("select count(1) as cnum" +
            " from spd.hos_out_balance_detail hobd" +
            " left join spd.hos_out_balance hob ON hob.id=hobd.pid" +
            " where hob.fill_date >= #{startTime} and hob.fill_date < #{endTime}")
    Integer selectCountByCond(FusionQueryVO query);
}
