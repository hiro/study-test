package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.FusionQueryVO;
import cn.minimelon.solon.domain.system.ReturnDetail;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface Spd2ReturnMapper extends BaseMapper<ReturnDetail> {
    @Select
    @Template("select rpb.id order_id,rpb.hos_id,rpb.hos_name,rpb.return_kind,rpb.prov_id,rpb.prov_name, " +
            "  ifnull(rpb.pur_mode,20) pur_mode, rpb.id order_no,rpb.fill_date return_time, " +
            "  rpbs.hos_goods_id goods_id,rpbs.erp_goods_code goods_code,rpbs.hos_goods_name goods_name,rpbs.goods_gg,rpbs.mfrs_name, " +
            "  rpbs.unit, IFNULL(rpbb.qty,rpbs.qty) as qty, rpbs.batch_code,rpbs.expdt_end_date expdt_date, " +
            "  so.branch_id, br.name as branch_name, db.sub_prov_id, bci.cname sub_prov_name, " +
            "  ifnull(dbl.send_price, 0) as price, dbl.id hos_batch_id, 2 as hos_version " +
            "from spd.return_prov_bill_sub rpbs  " +
            "inner join spd.return_prov_bill rpb on rpb.id = rpbs.bill_id " +
            "left join spd.return_prov_bill_batch rpbb on rpbs.id = rpbb.pid " +
            "left join spd.distr_bill db on db.id=rpbb.goods_batch_id " +
            "left join spd.distr_bill_list dbl on dbl.bill_id=rpbb.goods_batch_id and dbl.hos_goods_id=rpbb.goods_id " +
            "    and rpbs.source_bill_row=dbl.`row_number` " +
            "left join spd.bas_company_info bci on db.sub_prov_id=bci.id " +
            "left join spd.sys_org so on so.id=rpb.dept_id " +
            "left join spd.hos_branch_info br on br.id=so.branch_id " +
            "where rpb.fill_date >= #{startTime} and rpb.fill_date < #{endTime} order by rpbs.id limit #{skip}, #{pageSize}")
    List<ReturnDetail> selectListByCond(FusionQueryVO query);

    @Select
    @Template("select count(1) as cnum " +
            "from spd.return_prov_bill_sub rpbs  " +
            "inner join spd.return_prov_bill rpb on rpb.id = rpbs.bill_id " +
            "left join spd.return_prov_bill_batch rpbb on rpbs.id = rpbb.pid " +
            " where rpb.fill_date >= #{startTime} and rpb.fill_date < #{endTime}")
    Integer selectCountByCond(FusionQueryVO query);
}
