package cn.minimelon.solon.mapper.clear;

import cn.minimelon.solon.domain.system.FusionInvoiceDetail;
import org.beetl.sql.core.mapping.StreamData;
import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface FusionReportMapper {
    @Template("select id, detail_id from fusion_invoice_detail where invoicing_time < #{yearAgo}")
    StreamData<FusionInvoiceDetail> queryInvoiceDetail(@Param("yearAgo") String yearAgo);

    @Template("delete from fusion_invoice_detail where id in ( #{join(ids)} )")
    int deleteInvoiceDetail(@Param("ids") List<String> ids);
}
