package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.McmsProbeCollect;
import org.beetl.sql.mapper.BaseMapper;

public interface McmsProbeCollectMapper extends BaseMapper<McmsProbeCollect> {
}
