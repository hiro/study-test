package cn.minimelon.solon.mapper.system;

import cn.minimelon.solon.domain.system.FusionOutBalanceDetail;
import cn.minimelon.solon.domain.system.FusionQueryVO;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.Template;

import java.util.List;

public interface FusionOutBalanceMapper extends BaseMapper<FusionOutBalanceDetail> {
    @Select
    @Template("select count(1) as cnum" +
            " from fusion_out_balance_detail" +
            " where create_time >= #{startTime} and create_time < #{endTime}")
    Integer selectCountByCond(FusionQueryVO query);

    @Select
    @Template("select detail_id" +
            " from fusion_out_balance_detail" +
            " where detail_id in ( #{join(ids)} )")
    List<String> selectByDetailIds(List<String> ids);

    @Select
    @Template("select count(1)" +
            " from fusion_out_balance_detail" +
            " where detail_id = #{detailId}")
    Integer selectCountByDetailId(String detailId);
}
