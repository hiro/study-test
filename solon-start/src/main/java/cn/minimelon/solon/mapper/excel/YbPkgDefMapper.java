package cn.minimelon.solon.mapper.excel;

import cn.minimelon.solon.domain.excel.YbPkgDef;
import org.beetl.sql.mapper.BaseMapper;

public interface YbPkgDefMapper extends BaseMapper<YbPkgDef> {
}
