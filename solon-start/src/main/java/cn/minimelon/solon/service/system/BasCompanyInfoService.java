package cn.minimelon.solon.service.system;

import cn.minimelon.solon.domain.system.CompanyInfoVO;
import cn.minimelon.solon.domain.system.ESQueryVO;
import cn.minimelon.solon.domain.system.EsToken;

import java.util.List;

public interface BasCompanyInfoService {
    Long initEsData();

    List<CompanyInfoVO> queryEsData(ESQueryVO query);

    List<EsToken> analyzeWord(ESQueryVO query);
}
