package cn.minimelon.solon.service.system.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.minimelon.solon.domain.system.Spd2DistrBill;
import cn.minimelon.solon.domain.system.Spd3PurchaseDetail;
import cn.minimelon.solon.mapper.system.Spd3PurchaseMapper;
import cn.minimelon.solon.service.system.Spd3PurchaseService;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.solon.annotation.Db;
import org.noear.snack.ONode;
import org.noear.solon.annotation.ProxyComponent;

import java.io.File;
import java.util.HashMap;
import java.util.List;

@Slf4j
@ProxyComponent
public class Spd3PurchaseServiceImpl implements Spd3PurchaseService {

    @Db("writer")
    Spd3PurchaseMapper spd3PurchaseMapper;

    @Override
    public Integer updateExtInfo() {
        List<Spd3PurchaseDetail> list = spd3PurchaseMapper.selectUpdateList();
        for (int i = 0; i < list.size(); i++) {
            Spd3PurchaseDetail detail = list.get(i);
            log.info("info:{} data:{}", i, DateUtil.formatDate(detail.getCreateTime()));
            HashMap<String, String> extMap;
            if (StrUtil.isNotEmpty(detail.getExtInfo())) {
                extMap = ONode.deserialize(detail.getExtInfo(), HashMap.class);
            } else {
                extMap = new HashMap<>();
            }
            extMap.put("goodsCode", detail.getGoodsCode());
            extMap.put("goodsName", detail.getGoodsName());
            extMap.put("goodsGg", detail.getGoodsGg());
            detail.setExtInfo(ONode.stringify(extMap));
            log.info("extInfo:{}", detail.getExtInfo());
            spd3PurchaseMapper.updatePurDetail(detail);
        }
        spd3PurchaseMapper.updatePurBill();
        return list.size();
    }

    @Override
    public Integer updateDistrInfo() {
        List<Spd2DistrBill> list = spd3PurchaseMapper.selectDistrBillList();
        for(Spd2DistrBill bill : list) {
            log.info("id:{} rel1:{}", bill.getId(), bill.getBillRelationJson());
            HashMap<String, String> relationMap;
            if (StrUtil.isNotEmpty(bill.getBillRelationJson())) {
                relationMap = ONode.deserialize(bill.getBillRelationJson(), HashMap.class);
            } else {
                relationMap = new HashMap<>();
            }
            relationMap.put("purBillId", bill.getPurBillId());
            relationMap.put("purBillDate", DateUtil.formatDate(bill.getPurDate()));
            relationMap.put("confBillId", bill.getPurconfirmBillId());
            relationMap.put("distrBillId", bill.getId());
            relationMap.put("distrDate", DateUtil.formatDate(bill.getCreateDate()));
            bill.setBillRelationJson(ONode.stringify(relationMap));
            log.info("id:{} rel2:{}", bill.getId(), bill.getBillRelationJson());
        }
        return list.size();
    }

    @Override
    public Integer insertPrintInfo(String filePath) {
        File[] list = FileUtil.ls(filePath);
        int count = 0;
        for (File file : list) {
            String fileName = file.getName();
            if (fileName.contains("订单管理-") && !fileName.contains("默认")) {
                System.out.println(fileName + ">>>>");
                int idx = fileName.lastIndexOf('h');
                String hs = fileName.substring(0, idx);
                int idxS = hs.lastIndexOf('-');
                String hosId = fileName.substring(idx, fileName.length() - 4);
                String hosName = fileName.substring(idxS + 1, idx);
                if (hosId.indexOf("16") > 1) {
                    hosName = hosName + "单元包";
                } else if (hosId.indexOf("20") > 1) {
                    hosName = hosName + "高值";
                }
                System.out.println(hosId + ":" + hosName);
                String newName = ("distr_bill_" + hosId).replace('-', '_') + ".grf";
                // count = count + spd3PurchaseMapper.insertPrintInfo(hosId, hosName);
            }
        }
        return count;
    }
}
