package cn.minimelon.solon.service.system.thread;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Sheet;

import java.util.concurrent.CountDownLatch;

/**
 * 写Excel文件线程，必须指定sheet名称，start起始行，end结束行
 * 
 * @param <E>
 * 数据对象类型
 */
@Slf4j
public class ExcelWriter<E> implements Runnable {

    private final CountDownLatch doneSignal; // 线程完成信号

    private ExcelHandler<E> handler;

    private String serviceId;

    private int start;

    private int end;

    public ExcelWriter(CountDownLatch doneSignal, ExcelHandler<E> handler, String serviceId, int start, int end) {
        this.doneSignal = doneSignal;
        this.handler = handler;
        this.serviceId = serviceId;
        this.start = start;
        this.end = end;
    }

    @Override
    public void run() {
        try {
            for (int i = start; i < end; i++) {
                handler.writeRowContent(serviceId, i);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            doneSignal.countDown();
            log.info("start: {} end: {}  Count: {}", start, end, doneSignal.getCount());
        }
    }

}
