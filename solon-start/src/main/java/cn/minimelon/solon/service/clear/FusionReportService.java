package cn.minimelon.solon.service.clear;

public interface FusionReportService {
    void clearDetailOneYearAgo(String type);
}
