package cn.minimelon.solon.service.system.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.minimelon.solon.domain.system.FusionQueryVO;
import cn.minimelon.solon.domain.system.ReturnDetail;
import cn.minimelon.solon.mapper.system.FusionReturnDetailMapper;
import cn.minimelon.solon.mapper.system.Spd2ReturnMapper;
import cn.minimelon.solon.service.system.Spd2ReturnService;
import cn.minimelon.solon.service.system.thread.ExcelHandler;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.solon.annotation.Db;
import org.noear.solon.annotation.ProxyComponent;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.data.tran.TranPolicy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

@Slf4j
@ProxyComponent("spd2ReturnService")
public class Spd2ReturnServiceImpl implements Spd2ReturnService {
    private static final Integer PAGE_SIZE = 30000;

    @Db("master")
    private Spd2ReturnMapper spd2ReturnMapper;

    @Db("writer")
    private FusionReturnDetailMapper fusionReturnDetailMapper;

    @Override
    public Integer initFusionDetail(FusionQueryVO query) {
        ExcelHandler<Integer> handler = new ExcelHandler<>(query.getYear(), "spd2ReturnService");
        handler.multiThreadWrite();
        return null;
    }

    @Override
    public void insertMonthRangeSplit(DateTime startTime, DateTime endTime, String month) {
        FusionQueryVO query = new FusionQueryVO();
        query.setStartTime(DateUtil.formatDateTime(startTime));
        query.setEndTime(DateUtil.formatDateTime(endTime));
        query.setMonth(month);
        query.setPageSize(PAGE_SIZE);
        Integer count1 = spd2ReturnMapper.selectCountByCond(query);
        int pageNum = BigDecimal.valueOf(count1).divide(BigDecimal.valueOf(PAGE_SIZE), RoundingMode.CEILING).intValue();
        log.error("结算月：{} 数据量:{} 时间:{}~{}", query.getMonth(), count1, query.getStartTime(), query.getEndTime());
        for (int i = 0; i < pageNum; i++) {
            query.setSkip(i * PAGE_SIZE);
            try {
                List<ReturnDetail> detailList = spd2ReturnMapper.selectListByCond(query);
                if (CollUtil.isNotEmpty(detailList)) {
                    log.info(">>>>> 数量 {}, 开始执行", detailList.size());
                    List<List<ReturnDetail>> groupList = ListUtil.split(detailList, 1000);
                    insertSplitGroup(groupList);
                    log.info(">>>>> 数量 {}, 结束", detailList.size());
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                log.error("错误时间段：{}~{} 页码:{} msg:{}", query.getStartTime(), query.getEndTime(), i, ex.getMessage());
            }
        }
    }

    @Tran(policy = TranPolicy.requires_new)
    public void insertSplitGroup(List<List<ReturnDetail>> groupList) {
        for (List<ReturnDetail> group : groupList) {
            insertFusionDistrDetail(group);
        }
    }

    @Tran
    public void insertFusionDistrDetail(List<ReturnDetail> detailList) {
        for (ReturnDetail detail : detailList) {
            detail.setId(String.valueOf(IdUtil.getSnowflakeNextId()));
            detail.setVersion(0);
            detail.setCreateUser("0000");
            detail.setLastModified(new Date());
            detail.setLastModifiedUser("0000");
            detail.setHosVersion(2);
        }
        fusionReturnDetailMapper.insertBatch(detailList);
    }
}
