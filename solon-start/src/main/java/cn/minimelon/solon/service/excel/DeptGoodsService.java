package cn.minimelon.solon.service.excel;

import java.util.List;

public interface DeptGoodsService {

    void writeFile(String fileName) throws Exception;

    List<String> readFile(String fileName) throws Exception;
}
