package cn.minimelon.solon.service.excel;

import cn.minimelon.solon.domain.excel.YNExcelVO;

import java.util.List;

public interface PkgDefImportService {
    void start(String logicId, List<YNExcelVO> excelList);

    void refreshShelf(String logicId);
}
