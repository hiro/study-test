package cn.minimelon.solon.service;

import java.util.HashMap;
import java.util.Map;

public final class LocalCache {

    private static LocalCache cache = null;

    private Map<String, Object> hash;

    private LocalCache() {
        hash = new HashMap<>();
    }

    public static LocalCache getInstance() {
        if (cache == null) {
            cache = new LocalCache();
        }
        return cache;
    }

    public void setCache(String key, Object value) {
        hash.put(key, value);
    }

    public <T> T getCache(String key) {
        return (T) hash.get(key);
    }
}
