package cn.minimelon.solon.service.system;

import cn.hutool.core.date.DateTime;

public interface McmsProbeCollectService {
    void genCollectInfo();
    void genCollectForMonth(Integer year, Integer month, int start, int end);
    void genCollectForHour(DateTime time, Integer hour);
}
