package cn.minimelon.solon.service.system;

public interface Spd3PurchaseService {
    Integer updateExtInfo();

    Integer updateDistrInfo();

    Integer insertPrintInfo(String filePath);
}
