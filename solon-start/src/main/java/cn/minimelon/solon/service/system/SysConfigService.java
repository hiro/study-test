package cn.minimelon.solon.service.system;

import cn.minimelon.solon.domain.system.SysConfig;

import java.util.List;

public interface SysConfigService {
    Long insert(SysConfig config);

    Integer deleteById(String configId);

    Integer updateById(SysConfig config);

    SysConfig selectById(String configId);

    List<SysConfig> selectByIds(List<?> idList);

    List<SysConfig> selectByCond(SysConfig config);
}
