package cn.minimelon.solon.service.system.impl;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.minimelon.solon.domain.system.McmsProbeCollect;
import cn.minimelon.solon.mapper.system.McmsProbeCollectMapper;
import cn.minimelon.solon.service.system.McmsProbeCollectService;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.solon.annotation.Db;
import org.noear.solon.annotation.ProxyComponent;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.data.tran.TranPolicy;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

@Slf4j
@ProxyComponent
public class McmsProbeCollectServiceImpl implements McmsProbeCollectService {
    private static final String TIME_FMT = "%s-%02d-01 00:00:00";

    @Db("writer")
    McmsProbeCollectMapper probeCollectMapper;

    @Override
    public void genCollectInfo() {
        genCollectForMonth(2023, 10, 30, 31);
        genCollectForMonth(2023, 11, 0, 10);
    }

    @Override
    public void genCollectForMonth(Integer year, Integer month, int start, int end) {
        String timeStr = String.format(TIME_FMT, year, month);
        DateTime startTime = DateUtil.parseDateTime(timeStr);
        for (int day = start; day < end; day++) {
            DateTime offsetTime = DateUtil.offset(startTime, DateField.DAY_OF_MONTH, day);
            for (int hour = 0; hour < 24; hour++) {
                genCollectForHour(offsetTime, hour);
            }
        }
    }

    @Tran(policy = TranPolicy.requires_new)
    @Override
    public void genCollectForHour(DateTime time, Integer hour) {
        DateTime startTime = DateUtil.offset(time, DateField.HOUR_OF_DAY, hour);
        double humidity = RandomUtil.getRandom().nextDouble(50.0) + 20;
        double temperature = RandomUtil.getRandom().nextDouble(60) - 25.0D;
        for (int min = 0; min < 60; min = min + 5) {
            DateTime collectTime = DateUtil.offset(startTime, DateField.MINUTE, min);
            for (int i = 360001; i < 360005; i++) {
                McmsProbeCollect collect = buildCollectInfo(collectTime, i, temperature, humidity);
                probeCollectMapper.insert(collect);
                log.info("genCollectForHour >>>> collectTime: {}, min: {}", collectTime, i);
            }
        }
    }

    private McmsProbeCollect buildCollectInfo(DateTime collectTime, int id, double temperature, double humidity) {
        McmsProbeCollect collect = new McmsProbeCollect();
        collect.setId(IdUtil.getSnowflakeNextIdStr());
        collect.setCollectTime(new Date(collectTime.getTime()));
        collect.setCollectType(0);
        collect.setHosId("h0340");
        collect.setProbeId("" + id); // 待处理
        double dh = RandomUtil.getRandom().nextDouble(3.0) - 3;
        double dt = RandomUtil.getRandom().nextDouble(2.0) - 2;
        collect.setCollectHumidity(BigDecimal.valueOf(humidity + dh).setScale(4, RoundingMode.HALF_UP));
        collect.setCollectTemperature(BigDecimal.valueOf(temperature + dt).setScale(4, RoundingMode.HALF_UP));

        collect.setHumFlag(humidity > 30 ? 0 : 1);
        collect.setTemFlag(temperature > -5 && temperature <= 10 ? 1 : 0);
        return collect;
    }
}
