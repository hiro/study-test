package cn.minimelon.solon.service.system;

import cn.hutool.core.date.DateTime;
import cn.minimelon.solon.domain.system.FusionQueryVO;

public interface Spd2BaseService {
    Integer initFusionDetail(FusionQueryVO query);

    void insertMonthRangeSplit(DateTime startTime, DateTime endTime, String month);
}
