package cn.minimelon.solon.service.system.impl;

import cn.hutool.core.util.StrUtil;
import cn.minimelon.solon.domain.system.SysConfig;
import cn.minimelon.solon.mapper.system.SysConfigMapper;
import cn.minimelon.solon.service.system.SysConfigService;
import org.beetl.sql.core.query.LambdaQuery;
import org.beetl.sql.solon.annotation.Db;
import org.noear.solon.annotation.ProxyComponent;

import java.util.List;

@ProxyComponent
public class SysConfigServiceImpl implements SysConfigService {
    @Db
    SysConfigMapper configMapper;

    @Override
    public Long insert(SysConfig config) {
        configMapper.insert(config);
        return 1L;
    }

    @Override
    public Integer deleteById(String configId) {
        return configMapper.deleteById(configId);
    }

    @Override
    public Integer updateById(SysConfig config) {
        return configMapper.updateById(config);
    }

    @Override
    public SysConfig selectById(String configId) {
        return configMapper.single(configId);
    }

    @Override
    public List<SysConfig> selectByIds(List<?> idList) {
        return configMapper.selectByIds(idList);
    }

    @Override
    public List<SysConfig> selectByCond(SysConfig config) {
        LambdaQuery<SysConfig> query = configMapper.createLambdaQuery();
        if (StrUtil.isNotEmpty(config.getConfigName())) {
            query.andLike(SysConfig::getConfigName, "%" + config.getConfigName() + "%");
        }
        return query.select();
    }
}
