package cn.minimelon.solon.service.clear.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.minimelon.solon.domain.ClearEvent;
import cn.minimelon.solon.domain.system.FusionInvoiceDetail;
import cn.minimelon.solon.mapper.clear.FusionReportMapper;
import cn.minimelon.solon.service.clear.FusionReportService;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.core.mapping.StreamData;
import org.beetl.sql.solon.annotation.Db;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.event.EventBus;
import org.noear.solon.data.annotation.Tran;
import org.noear.solon.data.tran.TranPolicy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class FusionReportServiceImpl implements FusionReportService {
    @Db("master")
    private FusionReportMapper fusionReportMapper;

    @Tran(policy = TranPolicy.required)
    @Override
    public void clearDetailOneYearAgo(String type) {
        Date dateYear = DateUtil.offset(DateUtil.date(), DateField.YEAR, -1);
        String yearAgo = DateUtil.formatDateTime(DateUtil.beginOfDay(dateYear));
        log.info("一年前:{}", yearAgo);
        StreamData<FusionInvoiceDetail> streamData = fusionReportMapper.queryInvoiceDetail(yearAgo);
        // streamData.foreach(it -> log.info("ID:{} detail:{}", it.getId(), it.getDetailId()));
        List<String> idList = new ArrayList<>();
        FusionInvoiceDetail detail;
        do {
            detail = streamData.next();
            if (detail != null) {
                log.info("ID:{} detail:{}", detail.getId(), detail.getDetailId());
                idList.add(detail.getId());
                if (idList.size() == 500) {
                    ClearEvent clearEvent = new ClearEvent();
                    clearEvent.setType(type);
                    clearEvent.setIdList(ListUtil.toList(idList));
                    EventBus.publishAsync(clearEvent);
                    idList.clear();
                }
            } else if (CollUtil.isNotEmpty(idList)) {
                log.info("size:{}", idList.size());
                ClearEvent clearEvent = new ClearEvent();
                clearEvent.setType(type);
                clearEvent.setIdList(CollUtil.newArrayList(idList));
                EventBus.publishAsync(clearEvent);
                idList.clear();
            }
        } while (detail != null);
    }
}
