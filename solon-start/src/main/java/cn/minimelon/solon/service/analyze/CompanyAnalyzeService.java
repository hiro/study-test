package cn.minimelon.solon.service.analyze;

import cn.minimelon.solon.domain.analyze.CompanyGroupCross;
import cn.minimelon.solon.domain.analyze.CompanyGroupMax;
import cn.minimelon.solon.domain.analyze.CompanyGroupQuery;
import org.beetl.sql.core.page.PageResult;

import java.util.List;

public interface CompanyAnalyzeService {
    /**
     * 初始化分词，不能分词的需要手工介入
     *
     * @return 数据总数
     */
    Long initEsData();

    /**
     * 根据第一阶段分词，形成最大匹配分组
     */
    Long groupData();

    PageResult<CompanyGroupMax> queryPage(CompanyGroupQuery query);

    List<CompanyGroupCross> queryCross(CompanyGroupQuery query);
}
