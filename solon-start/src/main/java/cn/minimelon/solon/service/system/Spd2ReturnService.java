package cn.minimelon.solon.service.system;

import cn.hutool.core.date.DateTime;

public interface Spd2ReturnService extends Spd2BaseService {
    void insertMonthRangeSplit(DateTime startTime, DateTime endTime, String month);
}
