package cn.minimelon.solon.service.analyze;

public interface SimilarityService {
    double calculateScore(String doc1, String doc2);
}