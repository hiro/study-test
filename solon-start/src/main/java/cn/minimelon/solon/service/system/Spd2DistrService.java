package cn.minimelon.solon.service.system;

import cn.hutool.core.date.DateTime;

public interface Spd2DistrService extends Spd2BaseService {
    void insertMonthRangeSplit(DateTime startTime, DateTime endTime, String month);
}
