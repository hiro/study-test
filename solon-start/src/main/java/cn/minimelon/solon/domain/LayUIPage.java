package cn.minimelon.solon.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LayUIPage {
    private int page;
    private int limit;
}
