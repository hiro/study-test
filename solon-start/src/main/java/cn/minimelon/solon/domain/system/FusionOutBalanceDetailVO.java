package cn.minimelon.solon.domain.system;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author yujun
 * @date 2023-09-25 15:37
 */
@Data
public class FusionOutBalanceDetailVO extends FusionOutBalanceDetail {
    private String hosName;

    private String branchName;

    private BigDecimal spreadAmount;
}
