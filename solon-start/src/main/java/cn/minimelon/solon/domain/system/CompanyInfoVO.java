package cn.minimelon.solon.domain.system;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyInfoVO extends BaseCompanyInfo {
    public float _score;
}
