package cn.minimelon.solon.domain.excel;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PkgDefExcelVO {
    @ExcelProperty(index = 0)
    private Integer index;

    @ExcelProperty(index = 1)
    private String hosPrefix;

    @ExcelProperty(index = 2)
    private String shortId;

    @ExcelProperty(index = 3)
    private String hosGoodsId;

    @ExcelProperty(index = 4)
    private String goodsName;

    @ExcelProperty(index = 5)
    private String goodsGg;

    @ExcelProperty(index = 6)
    private String mfrsName;

    @ExcelProperty(index = 7)
    private String pkgName;

    @ExcelProperty(index = 8)
    private Double pkgQty;

    @ExcelProperty(index = 9)
    private String minQty;

    @ExcelProperty(index = 10)
    private String maxQty;

    @ExcelProperty(index = 11)
    private String diType;

    @ExcelProperty(index = 12)
    private String enableName;

    /**
     * 存在错误：0 否 1 是
     */
    @ExcelIgnore
    private int errorFlag = 0;
}
