package cn.minimelon.solon.domain.analyze;

import cn.minimelon.solon.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * 公司分组最大匹配
 */
@Getter
@Setter
@ToString
@Table(name = "company_group_max")
public class CompanyGroupMax extends BaseEntity {
    /**
     * ID
     */
    @AssignID("simple")
    private String id;
    /**
     * 分组词汇
     */
    private String groupWords;
    /**
     * 推荐名称
     */
    private String stdName;
    /**
     * 涉及ID
     */
    private String groupIds;
    /**
     * 涉及ID数两
     */
    private Integer groupCount;
}
