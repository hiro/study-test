package cn.minimelon.solon.domain.system;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FusionQueryVO {
    private Integer year;

    private String startTime;

    private String endTime;

    private Integer skip;

    private Integer pageSize;

    private String month;
}
