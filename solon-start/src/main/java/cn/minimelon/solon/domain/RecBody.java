package cn.minimelon.solon.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecBody {
    private String sign;
    private String data;
    private String timestamp;
    private String ownercode;
    private String appkey;
    private String pwd;
}
