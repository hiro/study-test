package cn.minimelon.solon.domain.system;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Getter
@Setter
@ToString
@Table(name = "distr_bill")
public class Spd2DistrBill {
    private String id;

    private String branchId;

    private String billRelationJson;

    private String purBillId;
    private String purconfirmBillId;

    private Date purDate;
    private Date createDate;
}
