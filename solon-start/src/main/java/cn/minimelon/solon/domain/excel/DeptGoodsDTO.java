package cn.minimelon.solon.domain.excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.Column;

@Getter
@Setter
@ToString
public class DeptGoodsDTO {
    private String id;
    private String goodsId;
    private String goodsName;
    private Integer packetCode;
    private String pkgDefId;
    @Column("small_id")
    private String smallId;
    @Column("small_qty")
    private Integer smallQty;
}
