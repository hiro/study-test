package cn.minimelon.solon.domain.excel;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

/**
 * 库房产品表;
 */
@Getter
@Setter
@ToString
@Table(name = "yb_goods")
public class YbGoods implements Serializable, Cloneable {
    /**
     * ID
     */
    @Column("id")
    private String id;
    /**
     * 医院ID
     */
    @Column("hos_id")
    private String hosId;
    /**
     * 虚拟仓ID;YB_LOGIC_STOCK.ID
     */
    @Column("logic_id")
    private String logicId;
    /**
     * 产品编码;（页面显示）HOS_GOODS_INFO.ID
     */
    @Column("goods_code")
    private String goodsCode;
    /**
     * 产品ID;HOS_GOODS_INFO.ID
     */
    @Column("goods_id")
    private String goodsId;
    /**
     * 产品名称
     */
    @Column("goods_name")
    private String goodsName;
    /**
     * 规格型号
     */
    @Column("goods_gg")
    private String goodsGg;
    /**
     * 主DI
     */
    @Column("goods_di")
    private String goodsDi;
    /**
     * 单位;最小计量单位
     */
    @Column("unit")
    private String unit;
    /**
     * 生产厂商ID
     */
    @Column("mfrs_id")
    private String mfrsId;
    /**
     * 生产厂商名称
     */
    @Column("mfrs_name")
    private String mfrsName;
    /**
     * 产地
     */
    @Column("made")
    private String made;
    /**
     * 产品SPD编码
     */
    @Column("spd_goods_code")
    private String spdGoodsCode;
    /**
     * 管理模式;10低值20高值
     */
    @Column("pur_mode")
    private String purMode;
    /**
     * 是否停采
     */
    @Column("can_purchase")
    private String canPurchase;
    /**
     * 院边仓管理默认0否1是
     */
    @Column("yb_flag")
    private String ybFlag;
    /**
     * 库存下限
     */
    @Column("min_qty")
    private Double minQty;
    /**
     * 库存上限
     */
    @Column("max_qty")
    private Double maxQty;
    /**
     * 状态;0停用1启用
     */
    @Column("tb_status")
    private Integer tbStatus;
    /**
     * 创建人;SYS_USER.ID
     */
    @Column("create_user")
    private String createUser;
    /**
     * 创建时间
     */
    @Column("create_time")
    private Date createTime;
    /**
     * 最后更新时间
     */
    @Column("last_modified")
    private Date lastModified;
    /**
     * 最后修改人;SYS_USER.ID
     */
    @Column("last_modified_user")
    private String lastModifiedUser;
    /**
     * 数据版本
     */
    @Column("version")
    private Integer version;

}