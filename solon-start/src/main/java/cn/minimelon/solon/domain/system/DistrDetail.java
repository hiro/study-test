package cn.minimelon.solon.domain.system;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Data
@Table(name = "fusion_distr_detail")
public class DistrDetail extends ReportBaseEntity {
    /**
     * 配送单id
     */
    private String orderId;
    /**
     * 配送单号
     */
    private String orderNo;
    /**
     * 配送时间
     */
    private Date distrTime;
    /**
     * 单价
     */
    private Double distrPrice;
    /**
     * 配送数量
     */
    private Double distrQty;
}
