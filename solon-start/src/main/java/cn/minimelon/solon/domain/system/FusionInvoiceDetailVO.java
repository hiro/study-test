package cn.minimelon.solon.domain.system;

import lombok.Data;

@Data
public class FusionInvoiceDetailVO extends FusionInvoiceDetail {

    private String hosName;

    private String branchName;
}
