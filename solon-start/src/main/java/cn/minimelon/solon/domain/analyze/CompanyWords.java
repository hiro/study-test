package cn.minimelon.solon.domain.analyze;

import cn.minimelon.solon.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * 公司名称分词
 */
@Getter
@Setter
@ToString
@Table(name = "company_words")
public class CompanyWords extends BaseEntity {
    /**
     * ID
     */
    @AssignID("simple")
    private String id;
    /**
     * 源ID
     */
    private String originId;
    /**
     * 源名称
     */
    private String originName;
    /**
     * 分词
     */
    private String wordList;
    /**
     * 分词数
     */
    private Integer workSize;
    /**
     * 公司类型
     */
    private Integer compKind;
}
