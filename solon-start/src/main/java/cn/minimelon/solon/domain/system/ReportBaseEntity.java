package cn.minimelon.solon.domain.system;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ReportBaseEntity extends Spd3BaseEntity {
    /**
     * 医院id
     */
    private String hosId;
    /**
     * 医院名称
     */
    private String hosName;
    /**
     * 院区id
     */
    private String branchId;
    /**
     * 院区名称
     */
    private String branchName;
    /**
     * 一级供应商id
     */
    private String provId;
    /**
     * 一级供应商名称
     */
    private String provName;
    /**
     * 二级供应商id
     */
    private String subProvId;
    /**
     * 二级供应商名称
     */
    private String subProvName;
    /**
     * 产品ID
     */
    private String goodsId;
    /**
     * 产品编号
     */
    private String goodsCode;
    /**
     * 产品名称
     */
    private String goodsName;
    /**
     * 规格型号
     */
    private String goodsGg;
    /**
     * 器械注册人
     */
    private String mfrsName;
    /**
     * 批号
     */
    private String batchCode;
    /**
     * 有效期至
     */
    private Date expdtDate;
    /**
     * 采购模式：20、高值；其他、单元包
     */
    private Integer purMode;
    /**
     * 单位
     */
    private String unit;
    /**
     * 数据源版本
     */
    private Integer hosVersion;

}
