package cn.minimelon.solon.domain.system;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Getter
@Setter
@ToString
@Table(name = "mcms_purchase_detail")
public class Spd3PurchaseDetail {
    private String id;

    /**
     * 产品id;mcms_goods_info.id
     */
    private String hosGoodsId;
    /**
     * 产品code
     */
    private String goodsCode;

    private String goodsName;

    private String goodsGg;

    private String extInfo;

    private Date createTime;
}
