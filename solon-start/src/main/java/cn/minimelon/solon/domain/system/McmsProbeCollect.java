package cn.minimelon.solon.domain.system;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "mcms_probe_collect")
public class McmsProbeCollect extends Spd3BaseEntity {
    /**
     * 医院ID;BAS_COMPANY_INFO.ID
     */
    @ApiModelProperty(name = "医院ID", notes = "BAS_COMPANY_INFO.ID")
    private String hosId;
    /**
     * 探针ID;采集设备的ID
     */
    @ApiModelProperty(name = "探针ID", notes = "采集设备的ID")
    private String probeId;
    /**
     * 采集方式;0探针上报1文本导入
     */
    @ApiModelProperty(name = "采集方式", notes = "0探针上报1文本导入")
    private Integer collectType;
    /**
     * 采集时间
     */
    @ApiModelProperty(name = "采集时间")
    private Date collectTime;
    /**
     * 采集温度
     */
    @ApiModelProperty(name = "采集温度")
    private BigDecimal collectTemperature;
    /**
     * 采集湿度
     */
    @ApiModelProperty(name = "采集湿度")
    private BigDecimal collectHumidity;
    /**
     * 温度标记;正常1异常0
     */
    @ApiModelProperty(name = "温度标记", notes = "正常1异常0")
    private Integer temFlag;
    /**
     * 湿度标记;正常1异常0
     */
    @ApiModelProperty(name = "湿度标记", notes = "正常1异常0")
    private Integer humFlag;
}
