package cn.minimelon.solon.domain.analyze;

import cn.minimelon.solon.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * 公司名称手工
 */
@Getter
@Setter
@ToString
@Table(name = "company_manual")
public class CompanyManual extends BaseEntity {
    /**
     * ID
     */
    @AssignID("simple")
    private String id;
    /**
     * 源ID
     */
    private String originId;
    /**
     * 源名称
     */
    private String originName;
    /**
     * 注册证号
     */
    private String regCode;
    /**
     * 标准名称
     */
    private String stdName;
    /**
     * 中文分词
     */
    private String cnWords;
    /**
     * 其他分词
     */
    private String exWords;
    /**
     * 公司类型
     */
    private Integer compKind;
}
