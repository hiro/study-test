package cn.minimelon.solon.domain.system;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class Spd3BaseEntity implements Serializable {
    private String id;
    private Integer version;
    private Date createTime;
    private String createUser;
    private Date lastModified;
    private String lastModifiedUser;
}
