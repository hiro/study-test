package cn.minimelon.solon.domain.analyze;

import cn.minimelon.solon.domain.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.AssignID;
import org.beetl.sql.annotation.entity.Table;

/**
 * 公司分组ID对照
 */
@Getter
@Setter
@ToString
@Table(name = "company_group_cross")
public class CompanyGroupCross extends BaseEntity {
    /**
     * ID
     */
    @AssignID("simple")
    private String id;
    /**
     * 分组ID
     */
    private String groupId;
    /**
     * 源头ID
     */
    private String originId;
    /**
     * 源头名称
     */
    private String originName;
    /**
     * 相似分数
     */
    private double score;
}