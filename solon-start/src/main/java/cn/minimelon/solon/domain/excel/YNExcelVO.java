package cn.minimelon.solon.domain.excel;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@ToString
public class YNExcelVO {
    @ExcelProperty(index = 0)
    private Integer index;

    @ExcelProperty(index = 1)
    private String branchGoodsId;

    @ExcelProperty(index = 2)
    private String hosGoodsId;

    @ExcelProperty(index = 3)
    private String goodsName;

    @ExcelProperty(index = 8)
    private String pkgName;

    @ExcelProperty(index = 9)
    private Double pkgQty;

    /**
     * 存在错误：0 否 1 是
     */
    @ExcelIgnore
    private int errorFlag = 0;
}
