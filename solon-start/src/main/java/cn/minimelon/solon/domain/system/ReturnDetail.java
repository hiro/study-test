package cn.minimelon.solon.domain.system;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Data
@Table(name = "fusion_return_detail")
public class ReturnDetail extends ReportBaseEntity {
    /**
     * 退货单id
     */
    private String orderId;
    /**
     * 退货单号
     */
    private String orderNo;
    /**
     * 退货类型
     */
    private Integer returnKind;
    /**
     * 退货时间
     */
    private Date returnTime;
    /**
     * 单位
     */
    private String unit;
    /**
     * 单价
     */
    private Double price;
    /**
     * 配送数量
     */
    private Double qty;
    /**
     * 入院批次ID
     */
    private String hosBatchId;
}
