package cn.minimelon.solon.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ClearEvent {
    private String type;
    private List<String> idList;
}
