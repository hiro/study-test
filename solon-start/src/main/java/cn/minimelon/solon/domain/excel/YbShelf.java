package cn.minimelon.solon.domain.excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

@Getter
@Setter
@ToString
@Table(name = "yb_shelf")
public class YbShelf {
    private String id;
    /**
     * 虚拟仓ID;LOGIC_STOCK.ID
     */
    @Column("logic_id")
    private String logicId;

    /**
     * 库位编码
     */
    private String shelfCode;

    /**
     * 库位名称
     */
    private String shelfName;

    /**
     * 库区类型;高值、低值、冷库、阴凉库、常温库
     */
    private Integer shelfType;

    /**
     * 库位类型;0库区1货位
     */
    @Column("stock_type")
    private Integer stockType;

    /**
     * 上级库位ID;YB_SHELF.ID
     */
    private String parentId;

    /**
     * 层级码;记录所有的父级ID
     */
    private String levelCode;

    /**
     * 状态;0停用1启用
     */
    private Integer tbStatus;

    /**
     * 逻辑删除;0正常1删除
     */
    private Integer delFlag;
}
