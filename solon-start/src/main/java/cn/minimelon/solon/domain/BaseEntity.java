package cn.minimelon.solon.domain;

import lombok.Getter;
import lombok.Setter;
import org.noear.snack.annotation.ONodeAttr;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class BaseEntity  implements Serializable {
    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    @ONodeAttr(format = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新者
     */
    private String updateBy;

    /**
     * 更新时间
     */
    @ONodeAttr(format = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 备注
     */
    private Integer revision;
}
