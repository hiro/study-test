package cn.minimelon.solon.domain.system;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yujun
 * @date 2023-09-25 15:36
 */
@Data
@Table(name = "fusion_out_balance_detail")
public class FusionOutBalanceDetail extends Spd3BaseEntity {

    private String detailId;

    private String hosId;

    private String hosName;

    private String branchId;

    private String hosGoodsId;

    private String goodsCode;

    private String goodsName;

    private String goodsGg;

    private Integer tbStatus;

    private String mfrsId;

    private String mfrsName;

    private String distrOrderNo;

    private String settleOrderNo;

    private BigDecimal distrPrice;

    private BigDecimal distrQty;

    private BigDecimal settlePrice;

    private BigDecimal settleQty;

    private BigDecimal invoiceQty;

    private BigDecimal settleAmount;

    private BigDecimal invoiceAmount;

    private Date accountDate;

    private String provId;

    private String provName;

    private String subProvId;

    private String subProvName;

    private Date operationTime;

    private Integer hosVersion;
}
