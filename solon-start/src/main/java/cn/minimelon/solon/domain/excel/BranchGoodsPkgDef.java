package cn.minimelon.solon.domain.excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.io.Serializable;
import java.util.Date;

/**
 * 医院商品单元包装定义表;
 */
@Getter
@Setter
@ToString
@Table(name = "branch_goods_pkg_def")
public class BranchGoodsPkgDef implements Serializable, Cloneable {
    /**
     * id;主键
     */
    @Column("id")
    private String id;
    /**
     * 逻辑仓id
     */
    @Column("logic_id")
    private String logicId;
    /**
     * 院区id
     */
    @Column("branch_id")
    private String branchId;
    /**
     * 产品id
     */
    @Column("goods_id")
    private String goodsId;
    /**
     * 包定义ID;院边包定义
     */
    @Column("yb_def_id")
    private String ybDefId;
    /**
     * 院区商品id
     */
    @Column("branch_goods_id")
    private String branchGoodsId;
    /**
     * 医院id
     */
    @Column("hos_id")
    private String hosId;
    /**
     * 医院商品id
     */
    @Column("hos_goods_id")
    private String hosGoodsId;
    /**
     * di类型;spd编码（默认）、ma、gs1
     */
    @Column("di_type")
    private String diType;
    /**
     * 码制风格;二维码（默认）、条码
     */
    @Column("code_type")
    private String codeType;
    /**
     * 使用单位di;默认di_type为spd编码时该值为bas_goods_info;的code
     */
    @Column("di_use")
    private String diUse;
    /**
     * 包含使用单位的数量
     */
    @Column("qty")
    private Double qty;
    /**
     * 包装级别;1-9;（1为最大箱）
     */
    @Column("level_code")
    private String levelCode;
    /**
     * 包装单位名;包装单位（箱、中包等）
     */
    @Column("level_name")
    private String levelName;
    /**
     * 数据生成时间
     */
    @Column("fill_date")
    private Date fillDate;
    /**
     * 数据版本
     */
    @Column("version")
    private Integer version;
    /**
     * 系统最后修改时间
     */
    @Column("last_update_datetime")
    private Date lastUpdateDatetime;
    /**
     * 是否是最大包装标识;1是;0否
     */
    @Column("max_pkg_flag")
    private Integer maxPkgFlag;
    /**
     * 上一级包装id;为根时该值为null
     */
    @Column("parent_pkg_def_id")
    private String parentPkgDefId;
    /**
     * 是否有效;0失效;1有效
     */
    @Column("eff_flag")
    private String effFlag;
    /**
     * 该单元包库存上限;备货量
     */
    @Column("pkg_uper_qty")
    private Double pkgUperQty;
    /**
     * 该单元包库存下限
     */
    @Column("pkg_low_qty")
    private Double pkgLowQty;
    /**
     * 创建人
     */
    @Column("create_user")
    private String createUser;
    /**
     * 创建时间
     */
    @Column("create_time")
    private Date createTime;
    /**
     * 最后更新人
     */
    @Column("last_modified_user")
    private String lastModifiedUser;
    /**
     * 最后更新时间
     */
    @Column("last_modified")
    private Date lastModified;
}
