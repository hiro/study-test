package cn.minimelon.solon.domain.excel;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

/**
 * 医院商品表;
 */
@Getter
@Setter
@ToString
@Table(name = "branch_goods_info")
public class BranchGoodsInfo implements Serializable, Cloneable {
    /**
     * ID
     */
    @Column("id")
    private String id;
    /**
     * 医疗机构ID
     */
    @Column("hos_id")
    private String hosId;
    /**
     * 院区id
     */
    @Column("branch_id")
    private String branchId;
    /**
     * 产品ID
     */
    @Column("goods_id")
    private String goodsId;
    /**
     * 操作人
     */
    @Column("operator")
    private String operator;
    /**
     * 创建时间
     */
    @Column("fill_date")
    private Date fillDate;
    /**
     * 数据版本
     */
    @Column("version")
    private Integer version;
    /**
     * 最后更新时间
     */
    @Column("last_update_datetime")
    private Date lastUpdateDatetime;
    /**
     * 是否有效;0失效;1有效
     */
    @Column("eff_flag")
    private String effFlag;
    /**
     * 是否包管理;0否;，1是
     */
    @Column("pkg_flag")
    private String pkgFlag;
    /**
     * 0;中心仓 1 院边仓
     */
    @Column("yb_flag")
    private Integer ybFlag;
}