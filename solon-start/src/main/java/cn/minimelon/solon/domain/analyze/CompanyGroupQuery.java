package cn.minimelon.solon.domain.analyze;

import cn.minimelon.solon.domain.LayUIPage;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CompanyGroupQuery extends LayUIPage {
    private String stdName;
    private String groupId;
}
