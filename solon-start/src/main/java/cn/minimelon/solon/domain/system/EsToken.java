package cn.minimelon.solon.domain.system;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EsToken {
    private String token;
    private String type;
    private Integer position;
    private Integer start_offset;
    private Integer end_offset;
}
