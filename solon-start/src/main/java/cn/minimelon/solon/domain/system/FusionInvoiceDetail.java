package cn.minimelon.solon.domain.system;

import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "fusion_invoice_detail")
public class FusionInvoiceDetail extends Spd3BaseEntity {

    private String detailId;

    private String invoiceNumber;

    private String invoiceCode;

    private String settleMonth;

    private String settleOrderNo;

    private String tbStatus;

    private String hosId;

    private String hosName;

    private String branchId;

    private String hosGoodsId;

    private String goodsCode;

    private String goodsName;

    private String goodsGg;

    private String batchCode;

    private String unit;

    private BigDecimal price;

    private BigDecimal invoiceQty;

    private BigDecimal invoiceAmount;

    private String provId;

    private String provName;

    private String subProvId;

    private String subProvName;

    private Date invoicingTime;

    private Date operationTime;

    private Integer hosVersion;
}
