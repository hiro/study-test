package cn.minimelon.solon.domain.utils;

import cn.minimelon.solon.domain.excel.*;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface ExcelBeanMapper {

    ExcelBeanMapper INSTANCE = Mappers.getMapper(ExcelBeanMapper.class);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "logicId", ignore = true),
            @Mapping(target = "goodsId", source = "id"),
            @Mapping(target = "goodsCode", source = "id"),
            @Mapping(target = "mfrsName", source = "hosMfrsName"),
            @Mapping(target = "tbStatus", source = "flag"),
    })
    YbGoods copyHosGoodsInfo(HosGoodsInfo hosGoodsInfo);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "branchId", ignore = true),
            @Mapping(target = "operator", source = "createUser"),
    })
    BranchGoodsInfo copyYBGoodsInfo(YbGoods goodsInfo);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "corpId", source = "hosId"),
            @Mapping(target = "goodsId", source = "id"),
    })
    YbPkgDef copyYBGoodsInfoDef(YbGoods goodsInfo);

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "hosId", source = "corpId"),
            @Mapping(target = "ybDefId", source = "id"),
            @Mapping(target = "hosGoodsId", source = "goodsCode"),
            @Mapping(target = "qty", source = "goodsQty"),
            @Mapping(target = "levelName", source = "pkgName"),
    })
    BranchGoodsPkgDef copyYBPkgDef(YbPkgDef pkgDef);
}
