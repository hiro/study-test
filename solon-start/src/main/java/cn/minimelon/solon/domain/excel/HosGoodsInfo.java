package cn.minimelon.solon.domain.excel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.InsertIgnore;
import org.beetl.sql.annotation.entity.Table;
import org.beetl.sql.annotation.entity.UpdateIgnore;

import java.io.Serializable;
import java.util.Date;

/**
 * 医院产品信息;
 */
@Getter
@Setter
@ToString
@Table(name = "hos_goods_info")
public class HosGoodsInfo implements Serializable, Cloneable {
    /**
     * ID
     */
    private String id;
    /**
     * 医院ID
     */
    private String hosId;
    /**
     * 供应商ID
     */
    private String provId;
    /**
     * 编号
     */
    private String code;
    /**
     *
     */
    private String goodsName;
    /**
     * 拼音码
     */
    private String shortPinyin;
    /**
     *
     */
    private String goodsGg;
    /**
     * 产地
     */
    private String made;
    /**
     * 生产厂商
     */
    private String mfrsId;
    /**
     * 院内厂商名称
     */
    private String hosMfrsName;
    /**
     * 单位
     */
    private String unit;
    /**
     * 包装
     */
    private Double packeage;
    /**
     * 单价
     */
    private Double price;
    /**
     * 唯一码管理策略
     */
    private String uniqueCodeStrategy;
    /**
     * 税率
     */
    private Double taxRate;
    /**
     * 统一码
     */
    private String masterCode;
    /**
     * 采购模式
     */
    private String purMode;
    /**
     * 是否计量
     */
    private String isMetering;
    /**
     * 品牌
     */
    private String brand;
    /**
     * 厂家出厂编码
     */
    private String mfrsCode;
    /**
     * 类别
     */
    private String kindCode;
    /**
     * 68分类
     */
    private String kind68code;
    /**
     *
     */
    private String erpCode;
    /**
     * 对应平台编码
     */
    private String spdGoodsCode;
    /**
     * 编码2
     */
    private String fieldCode2;
    /**
     * 编码3
     */
    private String fieldCode3;
    /**
     * 编码4
     */
    private String fieldCode4;
    /**
     * 类别属性
     */
    private String lbsx;
    /**
     * 备注信息
     */
    private String remark;
    /**
     * 标志
     */
    private String flag;
    /**
     *
     */
    private String uxid;
    /**
     * 建档时间
     */
    private Date fillDate;
    /**
     * 最后更新时间
     */
    private Date lastUpdateDatetime;
    /**
     * 数据版本
     */
    private Integer version;
    /**
     * 阳光采购价
     */
    private Double hitPrice;
    /**
     *
     */
    private String hitCode;
    /**
     * HIS销售价
     */
    private Double hisPrice;
    /**
     *
     */
    private String miName;
    /**
     * 医保编号
     */
    private String miCode;
    /**
     * 是否收费商品
     */
    private String charging;
    /**
     * 商品描述
     */
    private String goodsDesc;
    /**
     * 商品俗称
     */
    private String generalName;
    /**
     * 产品说明书附件
     */
    private String goodsDescFile;
    /**
     *
     */
    private String fileName;
    /**
     * 使用单位
     */
    private String useUnit;
    /**
     * 使用单位基数
     */
    private Double useUnitCount;
    /**
     *
     */
    private String salemanCode;
    /**
     *
     */
    private String salemanId;
    /**
     *
     */
    private String subPurMode;
    /**
     * 中包单位
     */
    private String midPackageUnit;
    /**
     * 使用人份
     */
    private Integer wishUseCount;
    /**
     * 开瓶有效时长
     * ;小时单位
     */
    private Integer openLiveCycle;
    /**
     * 医院产品状态
     */
    private Integer hosGoodsState;
    /**
     *
     */
    private Integer sendPackage;
    /**
     * 是否可采购
     */
    private String canPurchase;
    /**
     * 是否临采
     */
    private Integer tempPurchase;
    /**
     *
     */
    private Integer periodCount;
    /**
     * 试剂人份管理标识:;0-非人份管理 ; 1-人份管理
     */
    private Integer isIvd;
    /**
     * 是否洗消;需要洗消状态为1
     */
    private String decontaminationFalg;
    /**
     * 国家医保分类
     */
    private String icdCode;
    /**
     * 国家库单件名称
     */
    private String icdName;
    /**
     * 存储条件
     */
    private String storageConditions;
    /**
     * 国家医保耗材20代码
     */
    private String icd20code;
    /**
     * 贯标码
     */
    private String gbm;
    /**
     * 五省联动商品代码
     */
    private String provinceGoodsCode;
    /**
     * 五省联动商品id
     */
    private String provinceGoodsId;
    /**
     * 条码管理，0为E码;1为原厂码,2为UDI
     */
    private String barCodeMng;
    /**
     * 阳光平台编码
     */
    private String ygptCode;
    /**
     * 阳光平台主键编码
     */
    private String ygptPrimaryCode;
    /**
     * 是否线上采购
     */
    private String isOnline;
    /**
     * 阳光平台是否搭建配送关系
     */
    private String isDistrRel;
    /**
     * 线上采购类别
     */
    private String onlineKind;
    /**
     * 鑫卫网编码
     */
    private String xvmecCode;
    /**
     * 鑫卫网价格
     */
    private Double xvmecPrice;
    /**
     * 阳采ID
     */
    private String hitId;
    /**
     * 是否专科专用（0:否;1:是)
     */
    private String isSpecialized;

    @UpdateIgnore
    @InsertIgnore
    private String goodsDi;
}
