package cn.minimelon.solon.domain.system;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Getter
@Setter
@ToString
@Table(name = "bas_company_info")
public class BaseCompanyInfo {
    private String id;
    private String code;
    private String cname;
    private String regCode;
    private String jyxkCode;
    private Integer kind;
    @Column("last_update_datetime")
    private Date lastModified;
}
