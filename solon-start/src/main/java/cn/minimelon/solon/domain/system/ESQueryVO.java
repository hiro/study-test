package cn.minimelon.solon.domain.system;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ESQueryVO {
    /**
     * ES索引名称
     */
    private String index;
    /**
     * 输入的词
     */
    private String text;
    /**
     * 分词器
     */
    private String analyzer;
}
