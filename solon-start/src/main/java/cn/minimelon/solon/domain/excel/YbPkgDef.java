package cn.minimelon.solon.domain.excel;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

/**
 * 产品包定义;
 */
@Getter
@Setter
@ToString
@Table(name = "yb_pkg_def")
public class YbPkgDef implements Serializable, Cloneable {
    /**
     * ID
     */
    @Column("id")
    private String id;
    /**
     * 单位ID;YB_LOGIC_STOCK.ID
     */
    @Column("corp_id")
    private String corpId;
    /**
     * 虚拟仓ID;YB_LOGIC_STOCK.ID
     */
    @Column("logic_id")
    private String logicId;
    /**
     * 包类型;16：单元包;40:手术包
     */
    @Column("pkg_type")
    private String pkgType;
    /**
     * 包名称
     */
    @Column("pkg_name")
    private String pkgName;
    /**
     * 产品ID;仓库产品ID
     */
    @Column("goods_id")
    private String goodsId;
    /**
     * 产品编码;产品编码（HOS_GOODS_ID）
     */
    @Column("goods_code")
    private String goodsCode;
    /**
     * 产品数量;单元包必填
     */
    @Column("goods_qty")
    private Double goodsQty;
    /**
     * 库存下限;库存下限
     */
    @Column("min_qty")
    private Integer minQty;
    /**
     * 库存上限;库存上限
     */
    @Column("max_qty")
    private Integer maxQty;
    /**
     * DI类型;包码使用;spd编码（默认）、ma、gs1
     */
    @Column("di_type")
    private String diType;
    /**
     * 使用单位DI;包码使用;默认di_type为spd编码时该值为spd_code
     */
    @Column("di_use")
    private String diUse;
    /**
     * 码制风格;包码使用,0;二维码（默认）、1 条码
     */
    @Column("code_type")
    private String codeType;
    /**
     * 最大包装标识;1是;0否
     */
    @Column("max_pkg_flag")
    private Integer maxPkgFlag;
    /**
     * 状态;0停用1启用
     */
    @Column("tb_status")
    private Integer tbStatus;
    /**
     * 创建人;SYS_USER.ID
     */
    @Column("create_user")
    private String createUser;
    /**
     * 创建时间
     */
    @Column("create_time")
    private Date createTime;
    /**
     * 最后更新时间
     */
    @Column("last_modified")
    private Date lastModified;
    /**
     * 最后修改人;SYS_USER.ID
     */
    @Column("last_modified_user")
    private String lastModifiedUser;
    /**
     * 数据版本
     */
    @Column("version")
    private Integer version;
    /**
     * 逻辑删除;0正常1删除
     */
    @Column("del_flag")
    private Integer delFlag;
}
