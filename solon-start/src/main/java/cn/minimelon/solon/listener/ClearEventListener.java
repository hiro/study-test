package cn.minimelon.solon.listener;

import cn.minimelon.solon.domain.ClearEvent;
import cn.minimelon.solon.mapper.clear.FusionReportMapper;
import lombok.extern.slf4j.Slf4j;
import org.beetl.sql.solon.annotation.Db;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.event.EventListener;

@Slf4j
@Component
public class ClearEventListener implements EventListener<ClearEvent> {
    @Db("writer")
    private FusionReportMapper fusionReportMapper;

    @Override
    public void onEvent(ClearEvent clearEvent) throws Throwable {
        fusionReportMapper.deleteInvoiceDetail(clearEvent.getIdList());
    }
}
