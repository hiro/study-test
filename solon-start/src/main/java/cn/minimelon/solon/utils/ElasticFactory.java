package cn.minimelon.solon.utils;

import org.noear.esearchx.EsContext;
import org.noear.solon.Solon;

public class ElasticFactory {
    private ElasticFactory() {
    }

    private static EsContext instance;

    public static EsContext getInstance() {
        if (instance == null) {
            instance = new EsContext(Solon.cfg().getProperty("elastic.url"));
        }
        return instance;
    }
}
