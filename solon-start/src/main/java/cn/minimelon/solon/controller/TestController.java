package cn.minimelon.solon.controller;

import cn.minimelon.solon.domain.RecBody;
import cn.minimelon.solon.domain.excel.YNExcelVO;
import cn.minimelon.solon.service.LocalCache;
import cn.minimelon.solon.service.excel.DeptGoodsService;
import cn.minimelon.solon.service.excel.PkgDefImportService;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.util.ListUtils;
import lombok.extern.slf4j.Slf4j;
import org.noear.snack.ONode;
import org.noear.solon.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@Mapping("/test")
public class TestController {

    @Inject
    private PkgDefImportService pkgDefImportService;

    @Inject
    private DeptGoodsService deptGoodsService;

    @Get
    @Mapping("/show/{logicId}")
    public String show(@Param("logicId") String logicId) {
        LocalCache.getInstance().setCache("excelResult", new ArrayList<YNExcelVO>());
        String fileName = "D:/Z0Temp/PKG_DEF_SH_B0308.xlsx";
        // 这里 需要指定读用哪个class去读，然后读取第一个sheet 文件流会自动关闭
        EasyExcel.read(fileName, YNExcelVO.class, new ReadListener<YNExcelVO>() {
            /**
             * 单次缓存的数据量
             */
            public static final int BATCH_COUNT = 2000;
            /**
             *临时存储
             */
            private List<YNExcelVO> cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);

            @Override
            public void invoke(YNExcelVO data, AnalysisContext context) {
                cachedDataList.add(data);
                if (cachedDataList.size() >= BATCH_COUNT) {
                    saveData();
                    // 存储完成清理 list
                    cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
                }
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext context) {
                saveData();
            }

            /**
             * 加上存储数据库
             */
            private void saveData() {
                List<YNExcelVO> list = LocalCache.getInstance().getCache("excelResult");
                list.addAll(cachedDataList);
                log.info("logicId:{}", logicId);
                log.info("{}条数据，开始存储数据库！", list.size());
            }
        }).sheet().doRead();

        List<YNExcelVO> list = LocalCache.getInstance().getCache("excelResult");
        list.sort(Comparator.comparing(YNExcelVO::getHosGoodsId));
        pkgDefImportService.start(logicId, list);

        return "Solon欢迎您，请支持开源中国。";
    }

    @Get
    @Mapping("/refresh/shelf")
    public String refreshShelf() {
        pkgDefImportService.refreshShelf("L2022102600001");
        return "success";
    }

    @Get
    @Mapping("/update/dept_goods_info")
    public String updateDeptGoodsInfo() throws Exception {
        deptGoodsService.writeFile("/home/Hiro/ZXTemp/update" + System.currentTimeMillis() + ".sql");
        return "success";
    }

    @Post
    @Mapping("/rec")
    public String recTest(@Body RecBody req) {
        System.out.println(ONode.serialize(req));
        return "success";
    }
}
