package cn.minimelon.solon.controller.system;

import cn.minimelon.solon.domain.AjaxResult;
import cn.minimelon.solon.domain.system.SysConfig;
import cn.minimelon.solon.service.system.SysConfigService;
import org.noear.solon.annotation.*;

import java.util.List;

@Controller
@Mapping("system")
public class SysConfigController {

    @Inject
    SysConfigService configService;

    @Post
    @Mapping("config")
    public AjaxResult insert(SysConfig config) {
        Long count = configService.insert(config);
        return AjaxResult.success(count);
    }

    @Delete
    @Mapping("/config/{configId}")
    public AjaxResult delete(String configId) {
        Integer count = configService.deleteById(configId);
        return AjaxResult.success(count);
    }


    @Put
    @Mapping("config")
    public AjaxResult update(SysConfig config) {
        Integer count = configService.updateById(config);
        return AjaxResult.success(count);
    }

    @Get
    @Mapping("/config/{configId}")
    public AjaxResult query(String configId) {
        SysConfig config = configService.selectById(configId);
        return AjaxResult.success(config);
    }

    @Post
    @Mapping("/config/list")
    public AjaxResult list(SysConfig config) {
        List<SysConfig> list = configService.selectByCond(config);
        return AjaxResult.success(list);
    }
}
