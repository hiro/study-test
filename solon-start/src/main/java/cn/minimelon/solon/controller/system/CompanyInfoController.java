package cn.minimelon.solon.controller.system;

import cn.minimelon.solon.domain.AjaxResult;
import cn.minimelon.solon.domain.system.ESQueryVO;
import cn.minimelon.solon.service.system.BasCompanyInfoService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;

@Controller
@Mapping("company")
public class CompanyInfoController {
    @Inject
    private BasCompanyInfoService basCompanyInfoService;

    @Post
    @Mapping("init")
    public AjaxResult init() {
        Long count = basCompanyInfoService.initEsData();
        return AjaxResult.success(count);
    }

    @Post
    @Mapping("query")
    public AjaxResult query(ESQueryVO query) {
        Object list = basCompanyInfoService.queryEsData(query);
        return AjaxResult.success(list);
    }

    @Post
    @Mapping("analyze")
    public AjaxResult analyze(ESQueryVO query) {
        Object list = basCompanyInfoService.analyzeWord(query);
        return AjaxResult.success(list);
    }
}
