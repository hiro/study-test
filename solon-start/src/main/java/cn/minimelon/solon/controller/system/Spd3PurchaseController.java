package cn.minimelon.solon.controller.system;

import cn.minimelon.solon.domain.AjaxResult;
import cn.minimelon.solon.service.system.Spd3PurchaseService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;

@Controller
@Mapping("/spd3/purchase")
public class Spd3PurchaseController {

    @Inject
    private Spd3PurchaseService spd3PurchaseService;

    @Post
    @Mapping("/ext/info")
    public AjaxResult updateExtInfo() {
        Integer count = spd3PurchaseService.updateExtInfo();
        return AjaxResult.success(count);
    }

    @Post
    @Mapping("/distr/info")
    public AjaxResult updateDistrInfo() {
        Integer count = spd3PurchaseService.updateDistrInfo();
        return AjaxResult.success(count);
    }

    @Post
    @Mapping("/insertPrintInfo")
    public AjaxResult insertPrintInfo() {
        Integer count = spd3PurchaseService.insertPrintInfo("C:\\Users\\Melon\\Downloads\\spd2配送单打印20231121");
        return AjaxResult.success(count);
    }
}
