package cn.minimelon.solon.controller.system;

import cn.minimelon.solon.domain.AjaxResult;
import cn.minimelon.solon.domain.system.FusionQueryVO;
import cn.minimelon.solon.service.system.McmsProbeCollectService;
import cn.minimelon.solon.service.system.Spd2DistrService;
import cn.minimelon.solon.service.system.Spd2ReturnService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;

@Controller
@Mapping("/spd2/distr")
public class Spd2DistrController {

    @Inject("spd2DistrService")
    private Spd2DistrService spd2DistrService;

    @Inject("spd2ReturnService")
    private Spd2ReturnService spd2ReturnService;

    @Inject
    private McmsProbeCollectService probeCollectService;

    @Post
    @Mapping("/init/fusion/distr/detail")
    public AjaxResult initFusion(FusionQueryVO query) {
        Integer count = spd2DistrService.initFusionDetail(query);
        return AjaxResult.success(count);
    }

    @Post
    @Mapping("/init/fusion/rtn/detail")
    public AjaxResult initReturn(FusionQueryVO query) {
        Integer count = spd2ReturnService.initFusionDetail(query);
        return AjaxResult.success(count);
    }

    @Post
    @Mapping("/init/collect")
    public AjaxResult initCollect(FusionQueryVO query) {
        probeCollectService.genCollectInfo();
        return AjaxResult.success(0);
    }
}
