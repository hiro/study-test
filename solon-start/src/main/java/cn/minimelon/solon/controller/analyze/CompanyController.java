package cn.minimelon.solon.controller.analyze;

import cn.minimelon.solon.domain.AjaxResult;
import cn.minimelon.solon.domain.analyze.CompanyGroupCross;
import cn.minimelon.solon.domain.analyze.CompanyGroupMax;
import cn.minimelon.solon.domain.analyze.CompanyGroupQuery;
import cn.minimelon.solon.service.analyze.CompanyAnalyzeService;
import org.beetl.sql.core.page.PageResult;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;

import java.util.List;

@Controller
@Mapping("/analyze/company")
public class CompanyController {
    @Inject
    private CompanyAnalyzeService companyAnalyzeService;

    @Post
    @Mapping("init")
    public AjaxResult init() {
        Long count = companyAnalyzeService.initEsData();
        return AjaxResult.success(count);
    }

    @Post
    @Mapping("group")
    public AjaxResult group() {
        Object list = companyAnalyzeService.groupData();
        return AjaxResult.success(list);
    }

    @Post
    @Mapping("/list/page")
    public AjaxResult queryPage(CompanyGroupQuery query) {
        PageResult<CompanyGroupMax> result = companyAnalyzeService.queryPage(query);
        return AjaxResult.success(result);
    }

    @Post
    @Mapping("/list/cross")
    public AjaxResult queryCross(CompanyGroupQuery query) {
        List<CompanyGroupCross> list = companyAnalyzeService.queryCross(query);
        return AjaxResult.success(list);
    }
}
