package cn.minimelon.solon.controller.clear;

import cn.minimelon.solon.service.clear.FusionReportService;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Put;

@Slf4j
@Controller
@Mapping("/clear/report")
public class FusionReportController {
    @Inject
    private FusionReportService fusionReportService;

    @Put
    @Mapping("/detail/{type}")
    public String clearDetailYearAgo(String type) {
        fusionReportService.clearDetailOneYearAgo(type);
        return "success";
    }
}
