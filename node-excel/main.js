import JsonUtil from './json_file.mjs';
import ExcelUtil from './excel_file.mjs';

function lowerCaseDefKey(arr) {
  if (Array.isArray(arr)) {
    arr.forEach((item) => {
      if (item.defKey) {
        item.defKey = item.defKey.toLowerCase()
      }
    })
  }
}

const jsonObject = JsonUtil.readFileSync('D:\\03Work\\SPD3.0\\云仓项目\\SPD院边仓.chnr.json');
lowerCaseDefKey(jsonObject.profile.default.entityInitFields);
lowerCaseDefKey(jsonObject.entities);
jsonObject.entities.forEach((item) => {
  lowerCaseDefKey(item.fields);
});
JsonUtil.saveFileSync(jsonObject, 'D:\\03Work\\SPD3.0\\云仓项目\\SPD院边仓.chnr.json');