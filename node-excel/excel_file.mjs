import Excel from 'exceljs';

const ValueType = Excel.ValueType;

function readExcel(fileName) {
  let workbook = new Excel.Workbook();

  workbook.xlsx.readFile(fileName).then(function () {
    let worksheet = workbook.getWorksheet(1); //获取第一个worksheet

    worksheet.eachRow(function (row, rowNumber) {
      console.log(`行号：${rowNumber}`);
      let rowSize = row.cellCount;
      let numValues = row.actualCellCount;
      console.log(`单元格数量: ${rowSize}, 实际数量: ${numValues}`);
      row.eachCell(function (cell, colNumber) {
        if (cell.type === ValueType.Formula) {
          console.log(`第${rowNumber}行${colNumber}列结果：${cell.result}`);
        } else {
          console.log(`第${rowNumber}行${colNumber}列类型：${cell.type}, 内容：${cell.value}`);
        }
      });
    });
  });
}

function writeExcel(workbook, fileName) {
  workbook.xlsx.writeFile(fileName)
    .then(function () {
      console.log('write done')
    });
}

const ExcelUtil = {
  readExcel,
  writeExcel,
};
export default ExcelUtil;