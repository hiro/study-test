package cn.minimelon.study;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.minimelon.study.mapper")
public class MyStudyMain {

    public static void main(String[] args) {
        SpringApplication.run(MyStudyMain.class, args);
    }

}
