package cn.minimelon.study.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("TEST_FIELD")
public class FieldPO {

    private String tableName; // 表名
    private String defKey; // 英文代码
    private String defName; // 中文名称
    private String comment; // 注释
    private String defaultValue; // 默认值
    private String domain; // 类型
    private String notNull; // 非空
    private String type; // 类型
    private String len; // 长度
}
