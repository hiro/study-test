package cn.minimelon.study.mapper;

import cn.minimelon.study.pojo.FieldPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface FieldMapper extends BaseMapper<FieldPO> {
}
