package cn.minimelon.study;

import cn.minimelon.study.mapper.FieldMapper;
import cn.minimelon.study.pojo.FieldPO;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class MyStudyTest {

    @Autowired
    private FieldMapper fieldMapper;


    @Test
    public void test() {
        QueryWrapper<FieldPO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("table_name", "branch_goods_pkg_def");
        List<FieldPO> fieldList = fieldMapper.selectList(queryWrapper);
        for(FieldPO po : fieldList) {
            if (po.getType().contains("varchar")) {
                po.setDomain("SpdText");
                po.setType("VARCHAR");
                po.setLen("36");
            } else if (po.getType().contains("char")) {
                po.setDomain("SpdType");
                po.setType("CHAR");
                po.setLen("1");
            } else if (po.getType().contains("datetime")) {
                po.setDomain("DateTime");
                po.setType("DATETIME");
                po.setLen("");
            } else if (po.getType().contains("decimal")) {
                po.setDomain("SpdDecimal");
                po.setType("DECIMAL");
                po.setLen("18");
            } else if (po.getType().contains("int")) {
                po.setDomain("Int");
                po.setType("INT");
                po.setLen("");
            }
            po.setTableName(null);
        }
        System.out.println(JSON.toJSONString(fieldList));
    }

}
