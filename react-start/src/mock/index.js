/**
 * 模拟接口数据
 */
import Mock from 'mock2js';

const rules = {
  // Support type as Object and Array
  'GET /api/platform/areas': (req, res) => {
    setTimeout(() => {
      res.send([
        {id: '1', code: 'platform', name: '基础平台'},
        {id: '2', code: 'area1', name: '主题域1'},
        {id: '3', code: 'area2', name: '主题域2'},
        {id: '4', code: 'area3', name: '主题域3'},
      ])
    }, 200)
  },

  'GET /api/platform/menus/menu': (req, res) => {
    setTimeout(() => {
      res.send([
        {key: 'dashboard', path: "/platform/dashboard", name: '仪表盘', icon: 'DashboardOutlined', isLeaf: 1, children:[]},
        {key: 'sub1', name: '菜单1', icon: 'UserOutlined', isLeaf: 0, children: [
            { key: 'item1', path: "/platform/sub1/item1", name: '叶子1', icon: '', isLeaf: 1, children:[] },
            { key: 'item2', path: "/platform/sub1/item2", name: '叶子2', icon: '', isLeaf: 1, children:[] },
            { key: 'item3', path: "/platform/sub1/item3", name: '叶子3', icon: '', isLeaf: 1, children:[] },
            { key: 'item4', path: "/platform/sub1/item4", name: '叶子4', icon: '', isLeaf: 1, children:[] },
          ]},
        {key: 'sub2', name: '菜单2', icon: 'LaptopOutlined', isLeaf: 0, children: [
            { key: 'item5', path: "/platform/sub2/item5", name: '叶子1', icon: '', isLeaf: 1, children:[] },
            { key: 'item6', path: "/platform/sub2/item6", name: '叶子2', icon: '', isLeaf: 1, children:[] },
            { key: 'item7', path: "/platform/sub2/item7", name: '叶子3', icon: '', isLeaf: 1, children:[] },
            { key: 'item8', path: "/platform/sub2/item8", name: '叶子4', icon: '', isLeaf: 1, children:[] },
          ]},
      ])
    }, 200)
  },

  'POST /api/common/login': (req, res) => {
    setTimeout(() => {
      res.send({
        user: {
          id: 1,
          code: 'admin',
          name: '系统管理员',
          deptId: 'org01',
          deptName: '未知公司',
          level: 9,
        },
        token: 'QSD.JJDD.SSSS',
      })
    }, 200)
  },

  '/demo': (req, res) => {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('ok 123')
      }, 1000)
    })
  },
};

Mock.mockWithRules(rules);
