import React, {Fragment} from "react";
import DynamicRoute from "../../route/DynamicRoute";

/**
 * Dashboard 默认首页
 */
class MainContent extends React.Component {

  render() {
    let url = this.props.location.pathname;
    console.log("MainContent >>>>> " + url);
    return (
      <Fragment>
        <DynamicRoute {...this.props} />
      </Fragment>
    );
  }
}

export default MainContent
