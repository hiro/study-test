import http from "../../tools/http";
import config from "../../tools/config";

const api = {
  getAreaList(comp) {
    http().get(config.areaListUrl).then(res => {
      let areaList = res.data;
      if (areaList && areaList.length > 0) {
        this.getSubMenus(comp, 'platform');
        comp.setState({ areaList: areaList });
      }
    });
  },
  getSubMenus(comp, area) {
    http().get(config.menuListUrl + "/" + area).then(res => {
      let menuList = res.data;
      if (menuList && menuList.length > 0) {
        comp.setState({ menuList: menuList });
      }
    });
  }
};

export default api;
