/**
 * 首页
 */
import React from 'react';
import {Layout, Menu, Avatar} from 'antd';
import './index.css'
import config from "../../tools/config";
import storage from "../../tools/storage";
import api from "./api";
import icon from "../../tools/icon";
import MainContent from "../MainContent";
import {Link} from "react-router-dom";

const {SubMenu} = Menu;
const {Header, Sider, Footer} = Layout;

let ctxHeight = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight) - 84;

export default class MainLayout extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      userId: '',
      areaSelected: 'platform',
      area: null,
      areaList: [],
      menuList: [],
    };
  }

  componentDidMount() {
    let token = storage.getToken();
    if (token === '') {
      this.props.history.push('/login');
    } else {
      let user = storage.getLoginUser();
      if (this.state.areaList.length < 1 || this.state.userId !== user.id) {
        api.getAreaList(this);
      }
    }
  }

  handleClickArea({key}) {
    if (key === this.state.areaSelected) {
      return false;
    }
    this.setState({areaSelected: key});
    if (key !== 'platform') {
      this.props.history.push('/view/' + key + '/dashboard');
    } else {
      this.props.history.push('/platform/dashboard');
    }
    api.getSubMenus(this, key);
    return true;
  }

  renderArea() {
    return this.state.areaList.map(area => (
      <Menu.Item key={area.code} onClick={this.handleClickArea.bind(this)}>{area.name}</Menu.Item>
    ));
  }

  renderMenuItem(menu) {
    if (menu.menuType === 'C') {
      return (
        <Menu.Item key={menu.menuId}
            // icon={menu.icon === '' ? '' : icon.create(menu.icon)}
        ><Link to={menu.path}>{menu.menuName}</Link></Menu.Item>
      );
    } else if (menu.menuType === 'M') {
      return (
        <SubMenu key={menu.menuId}
                 icon={menu.parentId === '0' ? icon.create(menu.icon) : ''}
                 title={menu.menuName}>
          {this.renderMenu(menu.children)}
        </SubMenu>
      );
    }
    return null;
  }

  renderMenu(list) {
    if (list && list.length > 0) {
      return list.map(menu => this.renderMenuItem(menu));
    }
    return null;
  }

  render() {
    let url = this.props.location.pathname;
    console.log("MainLayout >>>>> " + url);
    return (
      <Layout className="main-layout">
        <Header className="header">
          <div className="logo-div">
            <img src={config.logo} className="logo" alt="logo"/>
            <span>小瓜瓜系统</span>
          </div>
          <Avatar size="large" style={{ backgroundColor: '#87d068' }} icon={icon.create('UserOutlined')} />
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={[this.state.areaSelected]}>
            {this.renderArea()}
          </Menu>
        </Header>
        <Layout>
          <Sider width={200} className="site-layout-background">
            <Menu
              mode="inline"
              defaultSelectedKeys={['dashboard']}
              defaultOpenKeys={['sub1']}
              style={{height: '100%', borderRight: 0}}
            >
              {this.renderMenu(this.state.menuList)}
            </Menu>
          </Sider>
          <Layout style={{padding: '0px'}}>
            <MainContent height={ctxHeight} {...this.props} />
          </Layout>
        </Layout>
      </Layout>
    )
  }
}
