/**
 * 数据：归档列表
 */
import React from 'react';
import {PageHeader, Statistic} from "antd";
import PageButton from "../Common/PageButton";
import {PAGE_TYPE} from "../Common/Constans";
import PageFilter from "../Common/PageFilter";

const renderContent = (pageInfo, dataDef, e) => (
  <PageFilter pageInfo={pageInfo} dataDef={dataDef} />
);

const extraContent = (
  <div
    style={{
      display: 'flex',
      width: 'max-content',
      justifyContent: 'flex-end',
    }}
  >
    <Statistic
      title="Status"
      value="Pending"
      style={{
        marginRight: 32,
      }}
    />
    <Statistic title="Price" prefix="$" value={568.08}/>
  </div>
);

const Content = ({children, extra}) => (
  <div className="content">
    <div className="main">{children}</div>
    <div className="extra">{extra}</div>
  </div>
);

export default class DataArchiveView extends React.Component {
  constructor(props, context) {
    super(props, context);
    const {area, def} = props.match.params;
    this.state = {
      pageInfo: {
        type: 'query',
        active: 'update',
        area: area,
        def: def,
      },
      dataDef: {
        code: 'yg',
        name: '员工',
        remark: '',
      },
    };
  }

  loadData() {

  }

  render() {
    const {pageInfo, dataDef} = this.state;
    // 加载定义
    return (
      <PageHeader
        className="site-page-header-responsive"
        title={dataDef.name + '(' + dataDef.code + ')' + PAGE_TYPE[pageInfo.type]}
        subTitle={dataDef.remark}
        extra={PageButton(pageInfo, this)}
        footer={null}
      >
        <Content>{renderContent(pageInfo, dataDef)}</Content>
      </PageHeader>
    )
  }
}
