/**
 * 数据：正式数据查询
 */
import React from 'react';
import {PageHeader, Statistic, Tabs} from "antd";
import PageButton from "../Common/PageButton";
import {PAGE_TYPE} from "../Common/Constans";
import PageFilter from "../Common/PageFilter";

const {TabPane} = Tabs;

const renderContent = (pageInfo, dataDef, e) => (
  <PageFilter pageInfo={pageInfo} dataDef={dataDef} />
);

const extraContent = (
  <div
    style={{
      display: 'flex',
      width: 'max-content',
      justifyContent: 'flex-end',
    }}
  >
    <Statistic
      title="Status"
      value="Pending"
      style={{
        marginRight: 32,
      }}
    />
    <Statistic title="Price" prefix="$" value={568.08}/>
  </div>
);

const Content = ({children, extra}) => (
  <div className="content">
    <div className="main">{children}</div>
    <div className="extra">{extra}</div>
  </div>
);

export default class DataQueryView extends React.Component {
  constructor(props, context) {
    super(props, context);
    const {area, def} = props.match.params;
    this.state = {
      pageInfo: {
        type: 'query',
        active: 'update',
        area: area,
        def: def,
      },
      dataDef: {
        code: 'yg',
        name: '员工',
        remark: '',
      },
    };
  }

  loadData() {

  }

  activeChange(key) {
    this.setState({pageInfo: { active: key}})
  }

  render() {
    const {pageInfo, dataDef} = this.state;
    // 加载定义
    return (
      <PageHeader
        className="site-page-header-responsive"
        title={dataDef.name + '(' + dataDef.code + ')' + PAGE_TYPE[pageInfo.type]}
        subTitle={dataDef.remark}
        extra={PageButton(pageInfo, this)}
        footer={
          <Tabs defaultActiveKey={pageInfo.active} onChange={this.activeChange} size="small">
            <TabPane tab="申请" key="update">
              <span>Details</span>
            </TabPane>
            <TabPane tab="历史" key="history">
              <span>Rule</span>
            </TabPane>
          </Tabs>
        }
      >
        <Content>{renderContent(pageInfo, dataDef)}</Content>
      </PageHeader>
    )
  }
}
