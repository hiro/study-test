import http from "../../../tools/http";

const defApi = {
  getDataDef(defCode, callback) {
    http().get("/a/datadef/area/one/" + defCode.toUpperCase()).then(res => {
        if (callback) {
          callback(res.data);
        }
    });
  }
};

export default defApi;
