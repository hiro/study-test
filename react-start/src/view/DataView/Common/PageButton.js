import {Button} from "antd";

/**
 * 数据页面按钮
 * @param pageInfo 页面信息
 * @param view 页面对象，按钮事件
 * @returns {JSX.Element[]}
 * @constructor
 */
const PageButton = (pageInfo, view) => {
  return (
    [
      <Button key="3" size={"small"}>Operation</Button>,
      <Button key="2" size={"small"}>Operation</Button>,
      <Button key="1" size={"small"} type="primary">Primary</Button>,
    ]
  );
};

export default PageButton;