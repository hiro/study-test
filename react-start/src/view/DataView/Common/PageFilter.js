import React from 'react';
import {
  QueryFilter,
  ProFormText,
  ProFormDatePicker,
  ProFormDateRangePicker,
  ProFormSelect,
} from '@ant-design/pro-form';

const buildFilterFields = (fieldList: []) => {
  return fieldList.map((item, index) => {
    return (<ProFormText name={item.code} label={item.name} />);
  });
};

const PageFilter = (props) => {
  const {dataDef} = props;
  return (
    <QueryFilter
      size={"small"}
      span={6}
      onFinish={async (values) => {
        console.log(values);
      }}
    >
      { buildFilterFields(dataDef.fieldList) }
    </QueryFilter>
  );
};

export default PageFilter;
