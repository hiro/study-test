import http from "../../tools/http";
import storage from "../../tools/storage";
import config from "../../tools/config";

const api = {
  postLogin(comp, values) {
    http().post(config.loginUrl, {data: values}).then(data => {
      storage.setToken(data.token);
      storage.setLoginUser(data.user);
      comp.props.history.push('/platform');
    });
  }
};

export default api;
