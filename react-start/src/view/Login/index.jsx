/**
 * 登录页面
 */
import React from 'react';
import {Form, Input, Button, Row, Col} from 'antd';
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import './index.css';
import config from '../../tools/config'
import storage from '../../tools/storage';
import api from "./api";

export default class LoginView extends React.Component {
  onFinish = (values) => {
    storage.removeItem(storage.TOKEN_KEY);
    storage.removeItem(storage.USER_KEY);
    api.postLogin(this, values)
  }

  onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  }

  render() {
    return (
      <Row className="login-page">
        <Col xs={22} sm={20} md={16} lg={6} xl={6} className="login-box">
          <div className="login-logo-row">
            <img src={config.logo} className="logo" alt="logo"/>
            <span>欢迎登录</span>
          </div>
          <div>
            <Form
              name="basic"
              wrapperCol={{span: 24}}
              initialValues={{remember: true}}
              onFinish={this.onFinish}
              onFinishFailed={this.onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                name="username"
                rules={[{required: true, message: '请输入用户名。'}]}
              >
                <Input placeholder="用户名/手机号" prefix={<UserOutlined/>}/>
              </Form.Item>
              <Form.Item
                name="password"
                rules={[{required: true, message: '请输入密码。'}]}
              >
                <Input.Password placeholder="请输入密码" prefix={<LockOutlined/>}/>
              </Form.Item>

              <Form.Item>
                <Button type="primary" htmlType="submit" className="login-form-button">
                  登录
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Col>
      </Row>
    )
  }
}
