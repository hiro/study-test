import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'antd/dist/antd.css';
import BasicRoute from "./route/BasicRoute";

ReactDOM.render(
  <BasicRoute/>,
  document.getElementById('root')
);
