import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import Dashboard from "../view/Dashboard";
import DataQueryView from "../view/DataView/Query";
import DataAuditView from "../view/DataView/Audit";
import DataUpdateView from "../view/DataView/Update";
import DataArchiveView from "../view/DataView/Archive";
import DataHistoryView from "../view/DataView/History";

const DynamicTest = ({match}) => {
  console.log(match.params)
  return (
    <div>
      欢迎光临：{match.params.area} / {match.params.def}
    </div>
  );
};

const DynamicRoute = (props) => (
  <div
    className="site-layout-background"
    style={{
      padding: 0,
      margin: 2,
      minHeight: 280,
      height: props.height,
    }}
  >
    <Redirect exact from="/platform" to="/platform/dashboard"/>
    <Route path="/platform/dashboard" component={Dashboard}/>
    <Route path="/platform/:area/:def" component={DynamicTest}/>
    <Route path="/view/:area/dashboard" component={DynamicTest}/>
    <Route path="/view/:area/:def/query" component={DataQueryView}/>
    <Route path="/view/:area/:def/audit" component={DataAuditView}/>
    <Route path="/view/:area/:def/update" component={DataUpdateView}/>
    <Route path="/view/:area/:def/archive" component={DataArchiveView}/>
    <Route path="/view/:area/:def/history" component={DataHistoryView}/>
  </div>
);


export default DynamicRoute;
