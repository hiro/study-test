import React from 'react';
import {HashRouter, Route, Switch, Redirect} from 'react-router-dom';
import MainLayout from "../view/MainLayout";
import RegisterView from "../view/Register";
import LoginView from "../view/Login";

const BasicRoute = () => (
  <HashRouter>
    <Switch>
      <Redirect exact from="/" to="/platform"/>
      <Route path="/platform" component={MainLayout}/>
      <Route path="/view" component={MainLayout}/>
      <Route path="/register" component={RegisterView}/>
      <Route path="/login" component={LoginView}/>
    </Switch>
  </HashRouter>
);


export default BasicRoute;
