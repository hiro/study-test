import React from "react";
import * as Icon from '@ant-design/icons';

const icon = {
  create(name) {
    console.log(name)
    if (!name || name === '#' || name === '') {
      return <Icon.RightOutlined/>;
    } else {
      return React.createElement(Icon[name]);
    }
  }
};

export default icon;
