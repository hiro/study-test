/**
 * 网页存储
 */
import config from "./config";

const TOKEN_KEY = config.basePrefix + 'Authorization';
const USER_KEY = config.basePrefix + 'User';

const getToken = function () {
  return sessionStorage.getItem(TOKEN_KEY) || '';
};

const setToken = function (token) {
  sessionStorage.setItem(TOKEN_KEY, token);
}

const getLoginUser = function () {
  let userStr = sessionStorage.getItem(USER_KEY);
  if (userStr == null) {
    return null;
  } else {
    return JSON.parse(userStr);
  }
}

const setLoginUser = function (user) {
  sessionStorage.setItem(USER_KEY, JSON.stringify(user));
}

const removeItem = function (key) {
  sessionStorage.removeItem(key);
}

const storage = {
  TOKEN_KEY,
  USER_KEY,
  getToken,
  setToken,
  getLoginUser,
  setLoginUser,
  removeItem,
};

export default storage;
