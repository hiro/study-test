window.cfg = {
    basePrefix: 'melon@',
    apiPrefix: '',
    logo: './logo.svg',
    loginUrl: '/common/login',
    areaListUrl: '/a/portal/areas',
    menuListUrl: '/a/portal/menus',
    tokenName: 'Authorization',
};
