const path = require('path');
const paths = require('react-scripts/config/paths');

module.exports = function override(config, env) {
  // 编译修改路径
  if (env === 'production') {
    // 修改path目录
    config.output.publicPath = "/app"
    paths.appBuild = path.join(path.dirname(paths.appBuild), '/build/app');
    config.output.path = path.join(path.dirname(config.output.path), '/build/app');
    console.log(config.output.path)
  }
  // console.log(`custom output path - ${config.output.path}`);
  config.devtool = false; // 关掉 sourceMap
  return config;
};
